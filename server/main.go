package main

import (
	"flag"
	//"net"
	"pokidocs/server/conn"
	"strconv"
)

var (
	// Execute Arguments
	// -addr [ip string]
	// -port [port int]
	addr = flag.String("addr", "localhost", "Address")
	port = flag.Int("port", 7777, "Port")
)

func main() {

	// Parse Execute Arguments
	flag.Parse()

	// Transform Execute Arguments into full IP Addr
	str_addr := *addr + ":" + strconv.Itoa(*port)
	//$src_addr, _ := net.ResolveTCPAddr("tcp", str_addr)

	// Execute Start Server Procedure
	//$ conn.ServerStart(src_addr)
	conn.ServerStart(str_addr)
}
