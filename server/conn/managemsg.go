package conn

import (
	"pokidocs/server/errserv"
	"pokidocs/server/store"
	"pokidocs/server/transform"
	"time"

	"github.com/mitchellh/mapstructure"
)

func ManageTransformMsg(msgWrap *MsgWrapper) error {
	if !(msgWrap.Client.IsSubscribed()) {
		return errserv.ErrClientNotSubscribed
	}

	var tf transform.DocTransform

	// Try to create DocTransform object from msgWrapper's Data field
	if err := mapstructure.Decode(msgWrap.Data, &tf); err != nil {
		return errserv.ErrDataIncorrect
	}

	if err := IsTransformMsgValid(msgWrap, &tf); err != nil {
		return err
	}

	// Wrap transformation
	tfWrap := &TfWrapper{
		Tf:     &tf,
		Client: msgWrap.Client,
	}

	// Send wrapped transformation into transform channel of specified Document's Manager
	go func(){
		msgWrap.Manager.docMngrs[msgWrap.Client.docID].transforms <- tfWrap
	}()
	time.Sleep(time.Millisecond * 50)

	return nil
}

func ManageOnlyIDMsg(msgWrap *MsgWrapper) (OnlyIDMsg, error) {
	var msg OnlyIDMsg

	// Try to create DocTransform object from msgWrapper's Data field
	if err := mapstructure.Decode(msgWrap.Data, &msg); err != nil {
		return OnlyIDMsg{}, errserv.ErrDataIncorrect
	}

	if err := IsOnlyIDMsgValid(msgWrap, &msg); err != nil {
		return OnlyIDMsg{}, err
	}

	return msg, nil
}

func ManageSubscribeMsg(msgWrap *MsgWrapper) error {
	if msgWrap.Client.IsSubscribed() {
		return errserv.ErrClientSubscribed
	}

	// Check if message is formed correctly by using
	// generic management function for OnlyID messages
	msg, err := ManageOnlyIDMsg(msgWrap)
	if err != nil {
		return err
	}

	// Subscribe client to document using Connection Manager's procedure
	msgWrap.Manager.SubscribeClient(msgWrap.Client, msg.DocumentID)

	return nil
}

func ManageUnsubscribeMsg(msgWrap *MsgWrapper) error {
	if !msgWrap.Client.IsSubscribed() {
		return errserv.ErrClientNotSubscribed
	}

	v, ok := msgWrap.Data.(map[string]interface{})

	if !ok || (len(v) > 0) {
		return errserv.ErrDataIncorrect
	}

	// Unsubscribe client to document using Connection Manager's procedure
	msgWrap.Manager.UnsubscribeClient(msgWrap.Client)

	return nil
}

func ManageRequestBlobMsg(msgWrap *MsgWrapper) error {
	if !msgWrap.Client.IsSubscribed() {
		return errserv.ErrClientNotSubscribed
	}

	// Check if message is formed correctly by using
	// generic management function for OnlyID messages
	msg, err := ManageOnlyIDMsg(msgWrap)
	if err != nil {
		return err
	}

	// Check if document is opened by server's Document Manager
	docMngr, ok := msgWrap.Manager.docMngrs[msg.DocumentID]
	if !ok {
		return errserv.ErrUnknownDocument
	}

	// Send client's data to Document Manager's channel.
	// This starts procedure of sending whole Document data to given Client
	go func(){
		docMngr.docRequests <- msgWrap.Client
	}()
	time.Sleep(time.Millisecond * 100)

	return nil
}

func ManageDocsInfoMsg(msgWrap *MsgWrapper) error {

	// Try to get Document info from Storage
	docInfo, err := msgWrap.Manager.store.GetDocInfo()
	if err != nil {
		return err
	}

	docsInfoMsg := &DocsInfoMsg{
		DocsInfo: docInfo,
	}

	// Send Documents Information to Client by using Connection Manager's Procedure
	msgWrap.Manager.SendDocsInfo(msgWrap.Client, docsInfoMsg)
	return nil
}

func ManageCreateDocMsg(msgWrap *MsgWrapper) error {
	var msg CreateDocMsg

	// Try to create DocTransform object from msgWrapper's Data field
	if err := mapstructure.Decode(msgWrap.Data, &msg); err != nil {
		return errserv.ErrDataIncorrect
	}

	if err := IsCreateDocMsgValid(msgWrap, &msg); err != nil {
		return err
	}

	// Create new Document object with specified name and empty data field
	doc := store.Document{
		Name: msg.Name,
		Data: "",
	}

	// Send document information to storage to execute save
	msgWrap.Manager.store.Create(doc)
	return nil
}

func ManageDeleteDocMsg(msgWrap *MsgWrapper) error {

	// Check if message is formed correctly by using
	// generic management function for OnlyID messages
	msg, err := ManageOnlyIDMsg(msgWrap)
	if err != nil {
		return err
	}

	// Try to delete given document
	if err := msgWrap.Manager.store.Delete(msg.DocumentID); err != nil {
		return err
	}

	if _, ok := msgWrap.Manager.docMngrs[msg.DocumentID]; ok {
		go func(){
			msgWrap.Manager.docMngrClose <- msg.DocumentID
		}()
	}

	return nil
}
