package conn

import (
	"encoding/json"
	"pokidocs/server/config"
	"pokidocs/server/errserv"
	"pokidocs/server/utils"
	"sort"
	"time"
)

var (
	// List of possible messages from clients to be processed by server
	// On side comments defined struct types of given message
	messageTypes = []string{
		"error",           // ErrorMsg
		"transform",       // transform.DocTransform
		"requestblob",     // OnlyIDMsg
		"blob",            // DataBlobMsg
		"subscribe",       // OnlyIDMsg
		"unsubscribe",     // nodata
		"requestdocsinfo", // nodata
		"docsinfo",        // DocsInfoMsg
		"createdoc",       // CreateDocMsg
		"deletedoc",       // OnlyIDMsg
	}
)

// Main wrapper over any message. It is used in server-client communication
// Fields with defined tag json:"-" are omitted in communication
type MsgWrapper struct {
	Typeof  string       `json:"typeof"` // Type of message
	Data    interface{}  `json:"data"`   // Data from message
	Client  *Client      `json:"-"`      // Pointer to Client who gets or sends this data
	Manager *ConnManager `json:"-"`      // Pointer to main Connection Manager
}

// Definitions of messages' structs
// These structs are inserted in `Data` field of MsgWrapper struct

// Data Blob Message used for getting whole document from server
type DataBlobMsg struct {
	DocumentID int    `json:"document_id" mapstructure:"document_id"`
	FullSize   int    `json:"full_size" mapstructure:"full_size"`
	Name       string `json:"name" mapstructure:"name"`
	Payload    string `json:"payload" mapstructure:"payload"`
	Version    int    `json:"version" mapstructure:"version"`
}

// Generic OnlyID Message used for every message
// that require only document ID to operate
type OnlyIDMsg struct {
	DocumentID int `json:"document_id" mapstructure:"document_id"`
}

// Docs Info Message defining Map document ID to its Name to provide
// metadata about every document available in server
type DocsInfoMsg struct {
	DocsInfo map[int]string `json:"docs_info" mapstructure:"docs_info"`
}

// Create Doc Message used for creating new document in server's database
type CreateDocMsg struct {
	Name string `json:"name" mapstructure:"name"`
}

// Function used to Create MsgWrapper object from given arguments
func WrapMessage(typeof string, obj interface{}, client *Client, manager *ConnManager) (MsgWrapper, error) {
	if ok := CheckMsgType(typeof); !ok {
		return MsgWrapper{}, errserv.ErrWrongMsgType
	}

	return MsgWrapper{
		Typeof:  typeof,
		Data:    obj,
		Client:  client,
		Manager: manager,
	}, nil
}

// Function used to Create MsgWrapper object from JSON string (located in `data` arg)
func WrapMessageFromJSON(data []byte, client *Client, manager *ConnManager) (MsgWrapper, error) {
	var msgWrap MsgWrapper

	if err := json.Unmarshal(data, &msgWrap); err != nil {
		return MsgWrapper{}, errserv.ErrMsgNotWrapped
	}

	if msgWrap.Typeof == "" || msgWrap.Data == nil {
		return MsgWrapper{}, errserv.ErrDataIncorrect
	}

	return WrapMessage(msgWrap.Typeof, msgWrap.Data, client, manager)
}

// Function used to Create JSON string from given args
func JSONFromWrapMessage(typeof string, obj interface{}) ([]byte, error) {
	msgWrap, err := WrapMessage(typeof, obj, &Client{}, &ConnManager{})
	if err != nil {
		return nil, err
	}

	// Try to create JSON string from MsgWrapper
	data, err := json.Marshal(msgWrap)
	if err != nil {
		return nil, errserv.ErrDataIncorrect
	}
	return data, nil
}

// Function used to check if field `Typeof` in MsgWrapper
// has one of types defined in `messageTypes` array
func CheckMsgType(typeof string) bool {
	sort.Strings(messageTypes)
	i := sort.SearchStrings(messageTypes, typeof)
	if i < len(messageTypes) && messageTypes[i] == typeof {
		return true
	}
	return false
}

// Main function to provide management over received messages from clients
// as it distributes management to expected procedures
func ManageMessages(msg []byte, client *Client, manager *ConnManager) error {
	var msgWrap MsgWrapper
	var err error
	err = nil

	if len(msg) > config.MaxDataSize {
		return errserv.ErrMsgTooBig
	}

	msgWrap, err = WrapMessageFromJSON(msg, client, manager)
	if err != nil {
		return err
	}

	// Main conditional statement which distribute management over message
	switch msgWrap.Typeof {
	case "transform":
		err = ManageTransformMsg(&msgWrap)

	case "requestblob":
		err = ManageRequestBlobMsg(&msgWrap)

	case "subscribe":
		err = ManageSubscribeMsg(&msgWrap)

	case "unsubscribe":
		err = ManageUnsubscribeMsg(&msgWrap)

	case "requestdocsinfo":
		err = ManageDocsInfoMsg(&msgWrap)

	case "createdoc":
		err = ManageCreateDocMsg(&msgWrap)

	case "deletedoc":
		err = ManageDeleteDocMsg(&msgWrap)

	case "docsinfo":
		err = errserv.ErrServerOnlyCommand

	case "blob":
		err = errserv.ErrServerOnlyCommand

	case "error":
		err = errserv.ErrServerOnlyCommand
	}
	return err
}

// Function used to send information to client about any error
// that was caught during processing its message
func HandleError(err error, client *Client) {
	data, _ := JSONFromWrapMessage("error", err)

	utils.Error(err)

	time.Sleep(10*time.Millisecond)
	select {
	case <-client.shutdown:
		return
	default:
		client.data <- data
	}
}
