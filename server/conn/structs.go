package conn

import (
	"net"
	"os"
	"pokidocs/server/store"
	"pokidocs/server/transform"
	"sync"
)

// Struct defining data about connected Client
type Client struct {
	docID    int           // Document ID Client has subscribed to
	addr     string        // IP Addr of Client
	sock     net.Conn      // Socket of Client
	data     chan []byte   // Channel of Data that awaits sending it to Client
	shutdown chan struct{} // Used For Shutting Down Connection
}

// Struct defining data related to Manager of given Document
type DocumentManager struct {
	docID       int                          // Document ID related to Document Manager
	connMngr    *ConnManager                 // Backward Pointer to Main Connector Manager
	store       store.SQL                    // Connector to SQL DB
	stack       *transform.DocTransformStack // Pointer to Stack of Transformation
	clients     map[*Client]bool             // List of Clients attached to Document Manager
	transforms  chan *TfWrapper              // Channel of Transforms
	docRequests chan *Client                 // Channel of Full Document Requests
	broadcast   chan BroadcastWrapper        // Channel used for sending data by broadcast
	close       chan struct{}                // Channel used for closing Document Manager
	mutex       sync.Mutex                   // Mutex used for synchronization
}

// Struct defining data related to Manager of server connections
type ConnManager struct {
	listener     net.Listener             // Main Server Listener
	store        store.SQL                // Connector to SQL DB
	clients      map[*Client]bool         // Map of Connected Clients
	docMngrs     map[int]*DocumentManager // Map of Connected Document Managers
	sigs         chan os.Signal           // Channel of OS Signals
	shutdown	 chan bool		  		  // Channel used for shutting down Server
	register     chan *Client             // Channel of Clients to be registered
	unregister   chan *Client             // Channel of Clients to be unregistered
	docMngrClose chan int                 // Channel for closing Document Managers
	wg           sync.WaitGroup           // WaitGroup used for Sync
	docMngrWg    sync.WaitGroup           // WaitGroup for docMngr sync
}

// Wrapper over Document Transform to include Pointer to client data
type TfWrapper struct {
	Tf     *transform.DocTransform
	Client *Client
}

// Wrapper over ANY message to include Pointer to client data
// Used when sending broadcast message to exclude client who has sent this data
type BroadcastWrapper struct {
	Message []byte
	Client  *Client
}
