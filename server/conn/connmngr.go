// Package defining most essential functions and objects for correct connection handling
package conn

import (
	"crypto/tls"
	"fmt"
	"net"
	"os"
	"os/signal"
	"pokidocs/server/errserv"
	"strconv"
	"sync"
	"syscall"
	"time"

	"pokidocs/server/config"
	"pokidocs/server/store"
	"pokidocs/server/utils"
)

// Function creating new Connection Manager
func NewConnManager(l net.Listener) (ConnManager, error) {

	// Create new Storage instance using config from config/config.go
	store, err := store.NewSQL(
		store.ParseDSN(
			config.DB_Username,
			config.DB_Password,
			config.DB_Host,
			config.DB_Port,
			config.DB_Database),
		store.NewTableConfig(),
	)
	if err != nil {
		return ConnManager{}, err
	}

	// Fields are explained in conn/structs.go file
	return ConnManager{
		listener:     l,
		store:        store,
		clients:      make(map[*Client]bool),
		docMngrs:     make(map[int]*DocumentManager),
		sigs:         make(chan os.Signal, 1),
		shutdown:     make(chan bool),
		register:     make(chan *Client),
		unregister:   make(chan *Client),
		docMngrClose: make(chan int),
		wg:           sync.WaitGroup{},
		docMngrWg:    sync.WaitGroup{},
	}, nil
}

// Main server listener loop and server start procedure
//$ func ServerStart(addr *net.TCPAddr) {
func ServerStart(addr string) {
	// Start listening on given IP Addr

	//$ l, err := net.ListenTCP("tcp", addr)
	cer, err := tls.LoadX509KeyPair("server.pem", "server.key")
	if err != nil {
		utils.Error(err)
		return
	}

	config := &tls.Config{
		Certificates: []tls.Certificate{cer},
	}
	l, err := tls.Listen("tcp", addr, config)
	if err != nil {
		utils.Error(err)
		return
	}
	fmt.Println("Listening...")
	manager, err := NewConnManager(l)
	if err != nil {
		utils.Error(err)
		return
	}

	// Start main Connection Manager loop
	go manager.manage()

	// Accept Connections Loop
	for {
		conn, err := l.Accept()
		if err != nil {
			break
		}

		test, ok := conn.(*tls.Conn)
		if !ok {
			conn.Close()
			continue
		}
		conn.SetReadDeadline(time.Now().Add(time.Second * 5))
		err = test.Handshake()
		if err != nil {
			utils.Error(err)
			conn.Close()
			continue
		}

		client := NewClient(conn, conn.RemoteAddr().String())

		// Send client data to be registered by Connection Manager
		go manager.RegisterClient(client)
	}
	// Wait for all Clients to be disconnected

	manager.wg.Wait()
	manager.docMngrWg.Wait()
	time.Sleep(time.Second)

	// Close all manager's channels
	close(manager.sigs)
	close(manager.shutdown)
	close(manager.register)
	close(manager.unregister)
}

// Function (main loop) for Managing Server's Connections
func (manager *ConnManager) manage() {

	// Bind sigs channel to selected signals
	signal.Notify(manager.sigs, syscall.SIGINT, syscall.SIGTERM)

	// Every 30second Ticker for synchronization
	ticker := time.NewTicker(30 * time.Second)
	defer ticker.Stop()
	synchroPacket := []byte("{}")

	// Main loop of Manager
	for {
		select {

		// Registering of New Client
		case client := <-manager.register:
			go manager.Read(client)
			go manager.Write(client)
			fmt.Println("Added new client! " + client.addr)

		// Unregistering of Disconnected Client
		case client := <-manager.unregister:
			if _, ok := manager.clients[client]; ok {
				manager.UnregisterClient(client)
			}

		// Unregistering of not used Document Managers
		case docID := <-manager.docMngrClose:
			if _, ok := manager.docMngrs[docID]; ok {
				manager.killDocMngr(manager.docMngrs[docID])
			}

		// If synchronization ticker fires
		case <-ticker.C:
			// Try to send synchronization packet to every client connected
			for client := range manager.clients {
				select {
				case <-client.shutdown:
					return
				default:
					client.data <- synchroPacket
				}
			}
			// Try to send ping to database checking whether db is still alive
			go func() {
				err := manager.store.DB.Ping()
				// If there was error while pinging, shutdown server, as it means db is down
				if err != nil {
					manager.shutdown <- true
				}
			}()

		// Start Shutdown Procedure when Sigs channel was filled
		case <-manager.sigs:
			fmt.Printf("Shutting Down from Signal\n")

			manager.kill()
			return

		// Start Shutdown Procedure when other goroutine sends shutdown sig
		case <-manager.shutdown:
			fmt.Printf("Shutting Down from critical Error\n")

			manager.kill()
			return

		default:
			time.Sleep(time.Millisecond * 20)
		}
	}
}

// Function used for killing Connection Manager
func (manager *ConnManager) kill() {
	manager.listener.Close()

	for client := range manager.clients {
		manager.UnregisterClient(client)
	}

	for _, docMngr := range manager.docMngrs {
		manager.killDocMngr(docMngr)
	}

	manager.wg.Wait()

	fmt.Println("Successfully Shutted Down all connections")
}

// Function used for killing Document Manager instance in controlled way
func (manager *ConnManager) killDocMngr(docMngr *DocumentManager) {

	for client := range docMngr.clients {
		manager.UnsubscribeClient(client)
	}

	// Attempt last Update of document
	if _, err := manager.store.ReadVer(docMngr.docID); err == nil {
		docMngr.mutex.Lock()
		docMngr.UpdateDoc()
		docMngr.mutex.Unlock()
	}

	// Close all channels related to Document Manager
	close(docMngr.transforms)
	close(docMngr.close)
	close(docMngr.broadcast)

	// Delete Document Manager from Connection Manager's map
	if _, ok := manager.docMngrs[docMngr.docID]; ok {
		delete(manager.docMngrs, docMngr.docID)
	}

	fmt.Println("DocMngr " + strconv.Itoa(docMngr.docID) + " has been terminated!")
	manager.docMngrWg.Done()
}

// Function used for registering Client to Connection Manager
func (manager *ConnManager) RegisterClient(client *Client) {
	manager.wg.Add(1)

	// Define registration to Connection Manager
	manager.clients[client] = true

	// Define client as unsubcribed
	client.docID = 0

	// Send signal to create new goroutines for Client management
	manager.register <- client
}

// Function used to unregister selected Client object
func (manager *ConnManager) UnregisterClient(client *Client) {

	// Send information to client about closure of connection (as error)
	HandleError(errserv.ErrConnectionClosed, client)
	time.Sleep(time.Millisecond * 30)

	// Delete selected Client from Manager's map
	manager.UnsubscribeClient(client)
	if _, ok := manager.clients[client]; ok {
		delete(manager.clients, client)
	}
	close(client.shutdown)
	time.Sleep(time.Millisecond * 15)

	// Close both channels from Client
	close(client.data)

	time.Sleep(time.Millisecond * 30)
	// Close client's socket
	client.sock.Close()

	time.Sleep(time.Millisecond * 150)
	manager.wg.Done()

	fmt.Println("A connection " + client.addr + " has been terminated!")
}

// Function used for subscribing client to Document
func (manager *ConnManager) SubscribeClient(client *Client, docID int) {

	// Create new Document Manager and start its loop
	// if there is not any attached to given document
	if _, ok := manager.docMngrs[docID]; !ok {
		manager.docMngrs[docID] = NewDocumentManager(docID, manager.store, manager)
		go manager.docMngrs[docID].Loop()
	}

	// Subscribe client to given Document
	client.docID = docID

	// Define registration of Client to Document Manager
	manager.docMngrs[docID].clients[client] = true

	// Attempt to sent whole document to Client
	go func() {
		if _, ok := manager.docMngrs[docID]; ok {
			manager.docMngrs[docID].docRequests <- client
		}
	}()
}

// Function used for unsubscribing client from Document
func (manager *ConnManager) UnsubscribeClient(client *Client) {
	if !(client.IsSubscribed()) {
		return
	}

	// Unsubcribe client from Document Manager
	if docMngr, ok := manager.docMngrs[client.docID]; ok {
		if _, ok := docMngr.clients[client]; ok {
			delete(manager.docMngrs[client.docID].clients, client)
		}
	}

	// Define client as unsubcribed
	client.docID = 0

	HandleError(errserv.ErrClientUnsubscribed, client)
}

// Function used to send metadata about every document in database
func (manager *ConnManager) SendDocsInfo(client *Client, docsInfoMsg *DocsInfoMsg) {
	data, _ := JSONFromWrapMessage("docsinfo", docsInfoMsg)

	go func() {
		select {
		case <-client.shutdown:
			return
		default:
			client.data <- data
		}
	}()
}
