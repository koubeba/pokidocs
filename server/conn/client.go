package conn

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"pokidocs/server/errserv"
)

// const defining end of string character
const jsonEnd   = byte(3)

// Function returning new client object
func NewClient(sock net.Conn, addr string) *Client {
	return &Client{
		docID:    0,
		sock:     sock,
		addr:     addr,
		data:     make(chan []byte),
		shutdown: make(chan struct{}),
	}
}

// Function checking whether client is Subscribed to any document
func (client *Client) IsSubscribed() bool {
	if client.docID > 0 {
		return true
	}
	return false
}

// Log packet from CLIENT to SERVER
func LogFrom(l *log.Logger, addr, msg string) {
	l.SetPrefix("[" + addr + "] " + time.Now().Format("15:04:05") + " :\n")
	l.Print(msg)
}

// Log packet from SERVER to CLIENT
func LogTo(l *log.Logger, msg string) {
	l.SetPrefix("[SERVER] " + time.Now().Format("15:04:05") + " :\n")
	l.Print(msg)
}

// Function used for reading string data from TCP Connection
func (manager *ConnManager) Read(client *Client) {

	l := log.New(os.Stdout, "", 0)
	// Initialize Reader from conn buffer
	reader := bufio.NewReader(client.sock)
	var buffer bytes.Buffer
	defer buffer.Reset()
ReadLoop:
	for {
		// Deadline for checking whether connection is still open
		err := client.sock.SetReadDeadline(time.Now().Add(10 * time.Millisecond))
		if err != nil {
			break ReadLoop
		}

		// Read lines from buffer until empty
		buff, err := reader.ReadString(jsonEnd)

		// If unexpected error occured or client was shutdown: disconnect the client
		if err != nil {
			if netErr, ok := err.(net.Error); ok && netErr.Timeout() {
				continue ReadLoop
			}
			select {
			case <-client.shutdown:
				break ReadLoop
			default:
				manager.unregister <- client
				break ReadLoop
			}
		}

		// Cut end of string character from buffer
		if len(buff) > 0 {
			buff = buff[:len(buff)-1]
		}
		buffer.Write([]byte(buff))

		if err := ManageMessages(buffer.Bytes(), client, manager); err != nil {
			HandleError(err, client)
			if err == errserv.ErrMsgTooBig {
				manager.unregister <- client
				break ReadLoop
			}
		}

		LogFrom(l, client.addr, buffer.String())

		buffer.Reset()
		time.Sleep(20 * time.Millisecond)
	}
	fmt.Println("Reader " + client.addr + " says bye")
}

// Function used for writing data from content string to TCP Connection buffer
func (manager *ConnManager) Write(client *Client) {

	// Initialize Writer to conn buffer
	l := log.New(os.Stdout, "", 0)

	// Packet for synchronization
	synchroPacket := []byte("{}")
	synchroPacket = append(synchroPacket, byte(3))
	
	writer := bufio.NewWriter(client.sock)
WriteLoop:
	for {
		select {

		// Quit func if Shutdown Channel has been closed
		case <-client.shutdown:
			break WriteLoop

		// Send data to client if something awaits
		case data, ok := <-client.data:

			if !ok {
				break WriteLoop
			}

			// Add end of message delimeter
			data = append(data, jsonEnd)

			_, err := writer.Write(data)
			if (string(data) != string(synchroPacket)) {
				LogTo(l, string(data))
			}
			// Flush Writer buffer if there are no errors
			if err == nil {
				err = writer.Flush()
			}

		default:
			time.Sleep(20 * time.Millisecond)
		}
	}
	fmt.Println("Writer " + client.addr + " says bye")
}
