package conn

import (
	"fmt"
	"pokidocs/server/errserv"
	"pokidocs/server/store"
	"pokidocs/server/transform"
	"pokidocs/server/utils"
	"strconv"
	"sync"
	"time"
)

// Function creating new Document Manager object and returning Pointer to it
func NewDocumentManager(docID int, sql store.SQL, connMngr *ConnManager) *DocumentManager {

	fmt.Println("Docmngr " + strconv.Itoa(docID) + " says hi")

	// Get document data
	doc, _ := sql.Read(docID)

	// Fields defined in conn/structs.go file
	return &DocumentManager{
		docID:       docID,
		connMngr:    connMngr,
		store:       sql,
		stack:       transform.NewDTStack(docID, doc.Version, len(doc.Data)),
		clients:     make(map[*Client]bool),
		transforms:  make(chan *TfWrapper),
		docRequests: make(chan *Client),
		broadcast:   make(chan BroadcastWrapper),
		close:       make(chan struct{}),
		mutex:       sync.Mutex{},
	}
}

// Main Document Manager's Loop
func (docMngr *DocumentManager) Loop() {

	docMngr.connMngr.docMngrWg.Add(1)

	fmt.Println("Docmngr loop says hi")
	time.Sleep(20 * time.Millisecond)

	// Start every half second ticker for Database updates
	ticker := time.NewTicker(500 * time.Millisecond)
	defer ticker.Stop()

	for {
		// Main Conditional statement of loop
		select {

		// Exit loop after receiving signal to this channel
		case <-docMngr.close:
			fmt.Println("Document Manager " + strconv.Itoa(docMngr.docID) + " is being terminated")
			return

		// Push transformation after receiving it to channel
		case tfWrapper := <-docMngr.transforms:

			docMngr.mutex.Lock()
			go func() {
				err := docMngr.PushTransform(tfWrapper)

				if err != nil {
					HandleError(err, tfWrapper.Client)
					// If error was made because of Desync, resend whole document to Client
					if err == errserv.ErrTfSkipped || err == errserv.ErrTfTooOld {
						go func() {
							docMngr.docRequests <- tfWrapper.Client
						}()
					}
				}
				docMngr.mutex.Unlock()
			}()

		// Update document after Ticker's signal
		case <-ticker.C:

			// First, try to ping database to check if it is alive
			err := docMngr.store.DB.Ping()
			// If err, it means db is down, so try to shutdown the server
			if err != nil {
				utils.Error(err)
				docMngr.connMngr.shutdown <- true
			}
			// Try to update the doc
			docMngr.mutex.Lock()
			err = docMngr.UpdateDoc()
			docMngr.mutex.Unlock()

			// If there was connection related to DB update, it means db is down
			if err == errserv.ErrDBUpdate {
				utils.Error(err)
				docMngr.connMngr.shutdown <- true
			}

		// Send whole document if client requested OR was desynced
		case client := <-docMngr.docRequests:

			docMngr.mutex.Lock()
			// TODO bug, client id -> 0

			// Get document
			doc, err := docMngr.GetDoc()
			docMngr.mutex.Unlock()

			if err != nil {
				HandleError(err, client)
			} else {
				docMngr.SendDoc(client, &doc)
			}

		// Broadcast transformations to other clients
		case broadcast := <-docMngr.broadcast:
			for client := range docMngr.clients {

				// If transformation comes from same client, do not resend it to him
				if client == broadcast.Client {
					continue
				}
				select {
				case <-client.shutdown:
					return
				default:
					client.data <- broadcast.Message
				}
			}

		default:
			// Close Document Manager if there are no clients connected to it
			if len(docMngr.clients) == 0 {
				fmt.Printf("No Clients attached to docMngr id %d, closing\n", docMngr.docID)
				docMngr.connMngr.docMngrClose <- docMngr.docID
				return
			}
			time.Sleep(20 * time.Millisecond)
		}
	}
}

// Function used for pushing new transformations to transformation stack
func (docMngr *DocumentManager) PushTransform(tf *TfWrapper) error {

	if tf.Tf.DocumentID != docMngr.docID {
		return errserv.ErrDocumentMismatch
	}

	// Try to push Transformation to stack
	if err := docMngr.stack.PushTransform(tf.Tf); err != nil {
		return err
	}

	// If Transformation was pushed to stack correctly, send it to other clients
	data, _ := JSONFromWrapMessage("transform", tf.Tf)

	msg := &BroadcastWrapper{
		Message: data,
		Client:  tf.Client,
	}

	go func() {
		docMngr.broadcast <- *msg
	}()

	return nil
}

// Function used for sending Whole Document Data to Client
func (docMngr *DocumentManager) SendDoc(client *Client, doc *store.Document) {
	content := string(doc.Data)

	blob := DataBlobMsg{
		DocumentID: docMngr.docID,
		FullSize:   len(content),
		Name:       doc.Name,
		Payload:    content,
		Version:    doc.Version,
	}

	data, _ := JSONFromWrapMessage("blob", blob)

	// Send data to Client
	time.Sleep(10 * time.Millisecond)
	select {
	case <-client.shutdown:
		return
	default:
		client.data <- data
	}

	// Send unprocessed transformations to Client
	for _, tf := range docMngr.stack.Unprocessed {
		data, _ := JSONFromWrapMessage("transform", tf)
		time.Sleep(10 * time.Millisecond)
		select {
		case <-client.shutdown:
			return
		default:
			client.data <- data
		}
	}
}

// Function used for getting Document information
func (docMngr *DocumentManager) GetDoc() (store.Document, error) {

	// Try to read Document Data from database
	doc, err := docMngr.store.Read(docMngr.docID)
	if err != nil {
		return store.Document{}, err
	}

	doc.ID = docMngr.docID

	return doc, nil
}

// Function used for Updating Document information
func (docMngr *DocumentManager) UpdateDoc() error {

	// Get document data before update
	pre_doc, err := docMngr.GetDoc()
	if err != nil {
		return err
	}
	content := []byte(pre_doc.Data)

	// Get all transformations from stack
	unprocessed := len(docMngr.stack.Unprocessed)
	processed := len(docMngr.stack.Processed)

	// Abort if there are no transformations
	if (processed + unprocessed) == 0 {
		return errserv.ErrDBNoTfs
	}

	// If there are unprocessed transformations, try to process them
	if unprocessed > 0 {
		docMngr.stack.SaveTransforms(&content)
	}

	// If there are processed transformations, try to flush old ones
	if processed > 0 {
		docMngr.stack.FlushSavedTransforms()
	}

	doc := store.Document{
		ID:      docMngr.docID,
		Data:    string(content),
		Version: docMngr.stack.Version,
	}

	// Update doc
	err = docMngr.store.Update(doc)
	if err != nil {
		return err
	}
	return nil
}
