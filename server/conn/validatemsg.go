package conn

import (
	"pokidocs/server/config"
	"pokidocs/server/errserv"
	"pokidocs/server/transform"
)

// Function checking whether Transformation Message is correctly formed
func IsTransformMsgValid(msgWrap *MsgWrapper, tf *transform.DocTransform) error {
	if !transform.IsValidTransform(*tf) {
		return errserv.ErrDataIncorrect
	}

	// Error if Client's subscribed document ID differs from message's one
	if msgWrap.Client.docID != tf.DocumentID {
		return errserv.ErrDocumentMismatch
	}

	return nil
}

// Function checking form of ANY OnlyID Message
func IsOnlyIDMsgValid(msgWrap *MsgWrapper, msg *OnlyIDMsg) error {

	// Error if document ID is negative
	if msg.DocumentID <= 0 {
		return errserv.ErrDataIncorrect
	}

	// Error if `ReadVer` Function exits with error
	if _, err := msgWrap.Manager.store.ReadVer(msg.DocumentID); err != nil {
		return errserv.ErrUnknownDocument
	}

	return nil
}

// Function checking form of CreateDoc Message
func IsCreateDocMsgValid(msgWrap *MsgWrapper, msg *CreateDocMsg) error {

	// Error if to-be-made Document Name is empty or exceeds maximum size
	if msg.Name == "" || len(msg.Name) > config.MaxDocNameSize {
		return errserv.ErrDataIncorrect
	}

	return nil
}
