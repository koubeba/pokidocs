// Package defining debug utils tools
package utils

import (
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"

	"errors"

	"github.com/sirupsen/logrus"
)

func Deb(msg ...interface{}) {
	pc, _, line, _ := runtime.Caller(1)
	f := runtime.FuncForPC(pc)
	fmt.Print("!!! " + f.Name() + " (" + strconv.Itoa(line) + "): ")
	for _, x := range msg {
		fmt.Print(x)
		fmt.Print(" ")
	}
	fmt.Print("\n")
}

//Fatal logs error and fails
func Fatal(errs ...error) {
	returnErrorEx(2, errs...) // ignore the return value
	fmt.Println("FATAL EXIT")
	os.Exit(1)
}

//Error logs errors in a unified way using logrus
func Error(errs ...error) {
	pc, file, line, _ := runtime.Caller(1)
	f := runtime.FuncForPC(pc)
	errStrs := make([]string, 0)
	for _, err := range errs {
		errStrs = append(errStrs, err.Error())
	}
	logrus.WithFields(logrus.Fields{
		"err":  strings.Join(errStrs, ", "),
		"line": line,
		"func": f.Name(),
		"file": file,
	}).Error()
}

//ReturnError logs error, but returns it, allowing returning of errors to be chained in a "stack trace"
func ReturnError(errs ...error) error {
	return returnErrorEx(2, errs...)
}

func returnErrorEx(caller int, errs ...error) error {
	newErrs := make([]error, 0)
	for _, v := range errs {
		if v != nil {
			newErrs = append(newErrs, v)
		}
	}
	errs = newErrs
	if len(errs) == 0 {
		return nil
	}
	pc, file, line, _ := runtime.Caller(caller)
	f := runtime.FuncForPC(pc)
	errStrs := make([]string, 0)
	for _, err := range errs {
		errStrs = append(errStrs, err.Error())
	}
	errStr := strings.Join(errStrs, ", ")
	logrus.WithFields(logrus.Fields{
		"err":  errStr,
		"line": line,
		"func": f.Name(),
		"file": file,
	}).Error("RETURNING")
	if len(errs) == 1 {
		return errs[0]
	}
	return errors.New(errStr)
}

func ReturnNewError(err string) error {
	return returnErrorEx(2, errors.New(err))
}

func PrintStack(all bool) {
	buf := make([]byte, 20000)
	runtime.Stack(buf, all)
	Deb(string(buf))
}
