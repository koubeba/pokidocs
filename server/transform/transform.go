// Package defining main functionality of server - transforming messages to get synchronized messages
// Main synchronization factor is time of arrival to server
package transform

import (
	"pokidocs/server/errserv"
	"strconv"
	"time"
)

// Function used to insert data from transformation to given content string
func ProcessTransform(content *[]byte, transform *DocTransform) error {

	var (
		start, middle, end []byte
		err                error
	)
	toDelete := 0

	if transform.Type == "insert" {
		// Error if location exceeds len of content
		if transform.Location > len(*content) {
			return errserv.ErrTfExceedsContentSize
		}

		// Split content by given location
		start = (*content)[:transform.Location]
		middle = []byte(transform.Data)
		end = (*content)[transform.Location:]

	} else if transform.Type == "delete" {

		// Convert data string into number of chars to be deleted
		toDelete, err = strconv.Atoi(transform.Data)
		if err != nil {
			return errserv.ErrTfWrongType
		}

		if toDelete < 0 {
			return errserv.ErrTfNegDel
		}

		if transform.Location+toDelete > len(*content) {
			return errserv.ErrTfOutOfBuff
		}

		// Split content by given location
		start = (*content)[:transform.Location]
		middle = []byte("")
		end = (*content)[transform.Location+toDelete:]

	} else {
		// Error if transform.Type is neither insert nor delete
		return errserv.ErrTfWrongType
	}

	// Error if deletion range exceeds content's length
	if (transform.Location + toDelete) > len(*content) {
		return errserv.ErrTfExceedsContentSize
	}

	startLen, middleLen, endLen := len(start), len(middle), len(end)

	// If split over document resulted in empty strings it means content is empty
	// Assign transformation data as new content and return it
	if startLen == 0 && endLen == 0 {
		(*content) = middle
		return nil
	}

	// Apply transformation in given location
	(*content) = make([]byte, startLen+middleLen+endLen)
	copy(*content, start)
	copy((*content)[startLen:], middle)
	copy((*content)[startLen+middleLen:], end)

	return nil
}

// Function used to update incoming transformation if there are out of sync.
// It is achieved by comparing current/incoming (cur) Transform to one of processed ones (pre).
// If there transformation is out of sync by many versions, procedure of getting it synced looks like:
//
// FixOutdatedTransform(new, oldTransformation1)
//
// FixOutdatedTransform(new, oldTransformation2)
//
// FixOutdatedTransform(new, oldTransformation3)
//
// ...And so on
func FixOutdatedTransform(cur, pre *DocTransform) {
	var (
		preContent           []byte
		preLen               int
		preDelete, curDelete int
	)

	// Get crucial information of previous transformation
	if pre.Type == "insert" {
		preContent = []byte(pre.Data)
		preLen = len(preContent)
	} else {
		preDelete, _ = strconv.Atoi(pre.Data)
	}

	// Only thing that might change from current transformation is its delete reach
	if cur.Type == "delete" {
		curDelete, _ = strconv.Atoi(cur.Data)
	}

	// If previous transformation was insert
	if pre.Type == "insert" {
		// Move location of current tf, if previous tf was located before current one
		if pre.Location <= cur.Location {
			cur.Location += preLen
		}

		// If previous transformation was delete
	} else {

		// If current transformation is insert
		if cur.Type == "insert" {

			// Move location of current tf, if previous tf was located before current one
			if pre.Location <= cur.Location {

				// Delete was made before current location, move current tf's location
				// by amount of chars deleted by previous tf
				if (pre.Location + preDelete) <= cur.Location {
					cur.Location -= preDelete
					// Else move location of current tf to previous one
				} else {
					cur.Location = pre.Location
				}
			}

			// If current transformation is delete
		} else {

			// Move location of current tf, if previous tf was located before current one
			if pre.Location <= cur.Location {

				// Delete was made before current location, move current tf's location
				// by amount of chars deleted by previous tf
				if (pre.Location + preDelete) <= cur.Location {
					cur.Location -= preDelete

					// Else it means that deletions overlap. Try to shorten current deletion reach
					// To match previous tf deletion reach and move current tf's location to previous one
				} else {
					left := intMin(curDelete, (pre.Location+preDelete)-cur.Location)
					curDelete -= left
					cur.Location = pre.Location
				}

				// Adjust current tf's deletion reach, if previous tf is in its range
			} else if (cur.Location + curDelete) > pre.Location {

				// Calculate location difference between tfs
				locDiff := pre.Location - cur.Location

				// Calculate excess
				excess := intMax(0, (curDelete - locDiff))

				// Reduce current tf's deletion reach if excess is bigger than previous tf's deletion reach
				if excess > preDelete {
					curDelete -= preDelete

					// Else it means that everything beyond previous tf's locations has been already deleted
				} else {
					curDelete = locDiff
				}
			}
		}
	}

}

// Function checking if there are any unprocessed transformations
func (stack *DocTransformStack) IsUnprocessed() bool {
	return len(stack.Unprocessed) > 0
}

// Function used for pushing new transform to document transformation stack
func (stack *DocTransformStack) PushTransform(transform *DocTransform) error {
	var toDelete int
	var err error

	if !IsValidTransform(*transform) {
		return errserv.ErrDataIncorrect
	}

	if transform.Type == "delete" {
		toDelete, err = strconv.Atoi(transform.Data)
		if err != nil {
			return errserv.ErrTfWrongType
		}
		if toDelete < 0 {
			return errserv.ErrTfNegDel
		}
	}

	processed, unprocessed := len(stack.Processed), len(stack.Unprocessed)

	// Calculate difference between stack's version and transform's version
	diff := stack.Version - transform.Version + 1

	// Error, if diff is too high (out of sync)
	if diff > processed+unprocessed {
		return errserv.ErrTfTooOld
	}

	// Error, if diff is negative (out of sync)
	if diff < 0 {
		return errserv.ErrTfSkipped
	}

	for i := processed + unprocessed - diff; i < processed; i++ {
		FixOutdatedTransform(transform, &stack.Processed[i])
		diff--
	}

	for i := unprocessed - diff; i < unprocessed; i++ {
		FixOutdatedTransform(transform, &stack.Unprocessed[i])
	}

	// Error if transform reach exceeds virtuallen of stack
	if (transform.Location + toDelete) > stack.virtuallen {
		return errserv.ErrTfOutOfBuff
	}

	// Update stack version
	stack.Version++

	// Assign new version to transform
	transform.Version = stack.Version

	// Append new transformation to unprocessed tfs array
	stack.Unprocessed = append(stack.Unprocessed, *transform)

	// Update stacks virtuallen
	if transform.Type == "insert" {
		stack.virtuallen += len(transform.Data)
	} else {
		stack.virtuallen -= toDelete
	}

	return nil
}

// Function used for saving and updating documents with unprocessed transformations
func (stack *DocTransformStack) SaveTransforms(content *[]byte) {

	for i := 0; i < len(stack.Unprocessed); i++ {
		ProcessTransform(content, &stack.Unprocessed[i])
		stack.Unprocessed[i].Timestamp = time.Now().Unix()
	}

	recentlyProcessed := stack.Unprocessed[:]
	stack.Unprocessed = []DocTransform{}

	stack.Processed = append(stack.Processed, recentlyProcessed...)
}

// Function used for flushing old processed transformations
func (stack *DocTransformStack) FlushSavedTransforms() {

	flushTimeBoundary := time.Now().Unix() - stack.TimeToFlush

	var tooNewProcessed []DocTransform

	for i := 0; i < len(stack.Processed); i++ {
		if stack.Processed[i].Timestamp > flushTimeBoundary {
			tooNewProcessed = stack.Processed[i:]
			break
		}
	}

	stack.Processed = tooNewProcessed
}
