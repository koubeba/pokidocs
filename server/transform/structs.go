package transform

// Struct defining Document Transformation entity
type DocTransform struct {
	Type       string `json:"type" mapstructure:"type"`               // Type of Transformation (insert or remove)
	DocumentID int    `json:"document_id" mapstructure:"document_id"` // Document ID
	Location   int    `json:"location" mapstructure:"location"`       // Location of change
	Data       string `json:"data" mapstructure:"data"`               // Data to change (when insert: string to insert, when delete: string with number of chars to delete)
	Version    int    `json:"version" mapstructure:"version"`         // Suggested version of transformation
	Timestamp  int64  `json:"-" mapstructure:"-"`                     // Timestamp when server processed transformation
}

// Struct defining stack of Document Transformations
type DocTransformStack struct {
	Document    int            // Document ID
	virtuallen  int            // Virtual Length of Document with regard of awaiting transformations
	Version     int            // Current Version of Document on stack
	Processed   []DocTransform // Array of Processed Transformations
	Unprocessed []DocTransform // Array of Unprocessed Transformations
	TimeToFlush int64          // Config field defining time after processed transformation are flushed
}

// Struct returning new Document Transform Stack
func NewDTStack(docID, version, virtuallen int) *DocTransformStack {
	return &DocTransformStack{
		Document:    docID,
		virtuallen:  virtuallen,
		Version:     version,
		Processed:   []DocTransform{},
		Unprocessed: []DocTransform{},
		TimeToFlush: 60,
	}
}

func intMin(left, right int) int {
	if left < right {
		return left
	}
	return right
}

func intMax(left, right int) int {
	if left > right {
		return left
	}
	return right
}

// Function checking if given Document Transformation is Valid
func IsValidTransform(tf DocTransform) bool {

	// False if type of transformation is not insert or delete
	if tf.Type != "insert" && tf.Type != "delete" {
		return false
	}

	// False if document ID is negative
	if tf.DocumentID <= 0 {
		return false
	}

	// False if location of change is negative
	if tf.Location < 0 {
		return false
	}

	// False if data len is 0 (means there is no data to be processed)
	if len(tf.Data) == 0 {
		return false
	}

	// False if Version is negative
	if tf.Version <= 0 {
		return false
	}

	return true
}
