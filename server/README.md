# Aplikacja Serwera

Serwer jest oparty na języku oprogramowania Golang i wykorzystuje 
jego standardowe biblioteki jak i bibliteki: 
- [logrus](https://github.com/sirupsen/logrus)
- [pq](https://github.com/lib/pq)
- [mapstructure](https://github.com/mitchellh/mapstructure)

## Aktualna Wersja: v1.0

Obsługa pakietów JSON, obsługa aktualizacji i transformacji dokumentu

## Dokumentacja kodu (GODOC)

Została wytworzona za pomocą narzędzia GODOC, które generuje HTML.  
Znajduje się ona w folderze [docs](/server/docs/)  
W tym [miejscu](/server/docs/pkg/pokidocs/) znajduje się główny [index.html](/server/docs/pkg/pokidocs/index.html)

## Instalacja i Uruchomienie

Ewentualnie, jeżeli chcemy uruchomić poprzez przekompilowanie kodu źródłowego, trzeba:

- Zainstalować [Docker](https://docs.docker.com/install/)  
- Zainstalować [Docker-Compose](https://docs.docker.com/compose/install/)  
- Zainstalować [Golang](https://golang.org/doc/install)  
- Golang wykorzystuje zasadę jednego workspace, który określony jest w env jako $GOPATH,
  więc trzeba wybrać dogodną ścieżkę poprzez ustawienie tej zmiennej środ.
- Pobieramy dependencies do golanga:  
  `go get -u github.com/sirupsen/logrus`  
  `go get -u github.com/lib/pq`  
  `go get -u github.com/mitchellh/mapstructure`
- Pobieramy kod źrodłowy serwera do i umieszczamy go gdzieś w folderze w katalogu
  `$GOPATH/src`
- Kompilujemy poleceniem:  
  `go build`
- Po tym poleceniu powinien pojawić się plik binarny o nazwie paczki (tj. `server`)

Aby uruchomić poprawnie serwer, nalezy:  
- Uruchomić Dockera ( jeżeli nie był uruchomiony )  
    - Wejść do folderu z [plikami Dockera](/server/store/docker/)  
    - Utworzyć obraz oraz od razu uruchomić kontener Docker'a z bazą danych poprzez polecenie:  
      `docker-compose up -d`  
    - (Wyłączenie): `docker-compose down`  
- Uruchomić Serwer  
    - W [katalogu](/server/) wykonać polecenie:  
      `./server`


## Obsługa Serwera

Serwer odczytuje wiadomości tylko zakończone na znak 0x03 !

Serwer nie zwraca na razie komunikatów o błędnych wiadomościach wysłanych do niego, dlatego warto patrzeć na logi serwera.

Tworzenie dokumentów jest na razie tylko na poziomie bazy SQL. Wstępnie będą utworzone 3 dokumenty o zerowym payloadzie.

Serwer pamięta transformacje 60 sekund wstecz. Jeżeli będzie dezsynchronizacja większa niż 60s, serwer odeśle pełny dokument wraz z aktualną wersją.

Client na razie nie działa z nowymi typami wiadomości (na razie próbuje tylko zmieniać dokumenty)

Serwer oczekuje wiadomości JSON owiniętych taką nakładką:

```json
{
    "typeof": "<typ>",
    "data" {}
}
```

Ułatwia to obsługę wiadomości.

Serwer posiada na razie obsługę takich wiadomości jak:

transform - Transformacja,  
```json
"data":
    {  
        "type": `string`,  
        "document_id": `int`,  #reduntantne jak jest subskrybcja?
        "location": `int`,  
        "data": `string`,  
        "version": `int`  
    } 
```

Serwer będzie posiadał obsługę takich wiadomości jak:

requestblob - Prosba o pelny dokument,  
`"data": {"document_id": int}`

subscribe - Prosba o dolaczenie do edycji dokumentu,  
`"data": {"document_id": int}`

unsubscribe - Prosba o odlaczenie od edycji dokumentu,  
`"data": {}`

requestdocsinfo - Prosba o metadane wszystkich dokumentów,  
`"data": {}`

createdoc - Prosba o utworzenie dokumentu,  
`"data": {"name": string}`

deletedoc - Prospa o usuniecie dokumentu,  
`"data": {"document_id": int}`


Serwer odsyła:

error - Info o błędzie z kodem  
`"data": {"code": int, "message":string}  

transform - Tak jak wyżej  

blob - Odpowiedź na requestblob  
```json
"data":
    {  
        "document_id": `int`,  
        "full_size": `int`,  
        "name": `string`,
        "payload": `string`,  
        "version": `int`  
    } 
```  

docsinfo - Odpowiedź na requestdocsinfo  
```json
"data":
    {
        "docs_info":
        {
            <id:string>: <name:string>,
            "1": "fajny doc",
            "2": "fajniejszy doc"
        }
    }
```