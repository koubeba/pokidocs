// Package defining most essential config settings
package config

const (
	// sizes
	MaxDocumentSize = 20000000 // 20 MB
	MaxDataSize     = 50000    // 50 KB
	MaxDocNameSize  = 200      // 20 Chars

	// db config
	DB_Username = "user"
	DB_Password = "pass"
	DB_Host     = "localhost"
	DB_Port     = "5432"
	DB_Database = "pokidocs"
)
