// Package defining all possible errors in server to be send to clients
// Error Codes:
// 100 - Transform Error
// 200 - Message Error
// 300 - Client Error
// 400 - DB Error
// 500 - Internal Error
// 600 - Connection Closed
package errserv

import (
	"fmt"
	"strconv"
)


// Struct defining errors coming out from Server
type ServerError struct {
	Code    int    `json:"code" mapstructure:"code"`       // Code of Error
	Message string `json:"message" mapstructure:"message"` // Message related to error
}

// Return error's string
func (error *ServerError) Error() string {
	return fmt.Sprintf("[%s]: %s", strconv.Itoa(error.Code), error.Message)
}

// Create new ServerError object
func NewServerError(code int, msg string) error {
	return &ServerError{
		Message: msg,
		Code:    code,
	}
}

var (
	ErrTfWrongType          = NewServerError(100, "Unknown Transform Type")
	ErrTfNegDel             = NewServerError(100, "Negative Delete Value")
	ErrTfNegLoc             = NewServerError(100, "Negative Location Value")
	ErrTfExceedsContentSize = NewServerError(100, "Location Exceeds Content Size")
	ErrTfTooOld             = NewServerError(100, "Transform Too Old")
	ErrTfSkipped            = NewServerError(100, "Transform Skipped")
	ErrTfOutOfBuff          = NewServerError(100, "Location or Deletion reach outreached doc")
	ErrTfWithNoPayload      = NewServerError(100, "Empty Transform")

	ErrDocumentMismatch = NewServerError(200, "Document ID mismatched")
	ErrMsgNotWrapped    = NewServerError(200, "Message is not wrapped correctly")
	ErrWrongMsgType     = NewServerError(200, "Wrong Message Type")
	ErrMsgTooBig        = NewServerError(200, "Message too big")
	ErrDataIncorrect    = NewServerError(200, "Data Given is Incorrect")
	ErrUnknownDocument  = NewServerError(200, "Unknown Document Requested")

	ErrClientSubscribed    = NewServerError(300, "Client already subscribed to document")
	ErrClientNotSubscribed = NewServerError(300, "Client not subscribed to document")
	ErrClientUnsubscribed  = NewServerError(300, "You have been Unsubscribed from document")

	ErrDBRead   = NewServerError(400, "SQL Read Failed")
	ErrDBCreate = NewServerError(400, "SQL Create Failed")
	ErrDBUpdate = NewServerError(400, "SQL Update Failed")
	ErrDBDelete = NewServerError(400, "SQL Delete Failed")
	ErrDBNoTfs	= NewServerError(400, "No Transformations to be saved")

	ErrUnknownCommand    = NewServerError(500, "Unknown Command")
	ErrServerOnlyCommand = NewServerError(500, "Server Only Command")

	ErrConnectionClosed = NewServerError(600, "Connection Closed")
)
