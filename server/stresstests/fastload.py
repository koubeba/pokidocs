import ssl
import socket
import time

host, port = '127.0.0.1', 7777

#sock.connect((host, port))
context = ssl._create_unverified_context()
#context = ssl.create_default_context()

formstr = '{"typeof":"subscribe", "data":{"document_id": 4}}'
test = formstr
ch = b'\x03'.decode("ascii")
unsub = '{"typeof":"unsubscribe", "data":{}}'

with socket.create_connection((host, port)) as sock:
    with context.wrap_socket(sock, server_hostname=host) as ssock:
        time.sleep(0.2)
        for i in range(0, 30):
            ssock.send((test + ch).encode('ascii'))
            #print(ssock.recv(2048))
            ssock.send((unsub + ch).encode('ascii'))
            time.sleep(0.05)
            #print(ssock.recv(2048))
