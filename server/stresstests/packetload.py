a = 1000*"123456789"
formstr = '{"typeof":"subscribe", "data":"%s"}'
test = formstr % a
ch = b'\x03'.decode("ascii")
b = 10000*"123456789"
test2 = formstr % b

import ssl
import socket
import time

host, port = '127.0.0.1', 7777

#sock.connect((host, port))
context = ssl._create_unverified_context()

with socket.create_connection((host, port)) as sock:
    with context.wrap_socket(sock, server_hostname=host) as ssock:
        ssock.send((test + ch).encode('ascii'))
        ssock.send((test2 + ch).encode('ascii'))