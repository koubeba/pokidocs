package store

// Struct defining Document (same as in Database)
type Document struct {
	ID      int    `json:"id"`      // ID of Document
	Name    string `json:"name"`    // Name of Document
	Data    string `json:"data"`    // Content of Document
	Version int    `json:"version"` // Current Version of Document
}

// Function returning new instance of Document
func NewDocument(id int, name, data string) Document {
	return Document{
		ID:      id,
		Name:    name,
		Data:    data,
		Version: 1,
	}
}
