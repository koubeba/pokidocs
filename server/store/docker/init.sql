CREATE DATABASE pokidocs
    WITH
    ENCODING = 'UTF8'
    LC_COLLATE = 'pl_PL.UTF-8'
    LC_CTYPE = 'pl_PL.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    TEMPLATE template0;

\c pokidocs

SET timezone='UTC-2';

CREATE TABLE document
(
    id serial PRIMARY KEY,
    name varchar NOT NULL,
    data text NOT NULL,
    version int NOT NULL DEFAULT 1
);

INSERT INTO document (name, data) VALUES ('test1', '');
INSERT INTO document (name, data) VALUES ('test2', '');
INSERT INTO document (name, data) VALUES ('test3', '');

