// Package defining document storage in Database
package store

import (
	"database/sql"
	"fmt"

	"pokidocs/server/errserv"

	_ "github.com/lib/pq"
)

// Function used to parse DataBase DSN used to connect server to DB
func ParseDSN(user, pass, addr, port, dbname string) string {
	dsnstr := "postgresql://%s:%s@%s:%s/%s?sslmode=disable"

	return fmt.Sprintf(dsnstr,
		user, pass, addr, port, dbname)
}

// Struct containing config of table holding documents
type TableConfig struct {
	TableName  string // Name of the table
	ColID      string // Name of the column holding document IDs
	ColName    string // Name of the column holding document names
	ColData    string // Name of the column holding document content of document
	ColVersion string // Name of the column holding document version
}

// Function returning new Table Config object
func NewTableConfig() TableConfig {
	return TableConfig{
		TableName:  "document",
		ColID:      "id",
		ColName:    "name",
		ColData:    "data",
		ColVersion: "version",
	}
}

// Struct containing vital data for connection to database
// and pointers to SQL statements that will be used by server
type SQL struct {
	dsn     string
	tconfig TableConfig
	DB      *sql.DB

	createStmt      *sql.Stmt
	updateStmt      *sql.Stmt
	readStmt        *sql.Stmt
	getDocsInfoStmt *sql.Stmt
	readVerStmt     *sql.Stmt
	deleteStmt      *sql.Stmt
}

// Function returning new SQL connection struct
func NewSQL(dsn string, tconfig TableConfig) (SQL, error) {

	// Try to connect to postgresql Database
	DB, err := sql.Open("postgres", dsn)
	if err != nil {
		return SQL{}, err
	}
	err = DB.Ping()
	if err != nil {
		return SQL{}, err
	}
	var (
		createStr, updateStr, deleteStr     string
		readStr, readVerStr, getDocsInfoStr string
		create, update, delete              *sql.Stmt
		read, readVer, getDocsInfo          *sql.Stmt
	)

	// Format strings containing SQL statements
	createStr = "INSERT INTO %v (%v, %v, %v) VALUES ($1, $2, $3)"
	updateStr = "UPDATE %v SET %v = $1, %v = $2 WHERE %v = $3"
	deleteStr = "DELETE FROM %v WHERE %v = $1"
	readStr = "SELECT %v, %v, %v FROM %v WHERE %v = $1"
	readVerStr = "SELECT %v FROM %v WHERE %v = $1"
	getDocsInfoStr = "SELECT %v, %v FROM %v"

	// Prepare Create SQL statement from string
	create, err = DB.Prepare(fmt.Sprintf(createStr,
		tconfig.TableName, tconfig.ColName, tconfig.ColData, tconfig.ColVersion,
	))
	if err != nil {
		return SQL{}, err
	}

	// Prepare Update SQL statement from string
	update, err = DB.Prepare(fmt.Sprintf(updateStr,
		tconfig.TableName, tconfig.ColData, tconfig.ColVersion, tconfig.ColID,
	))
	if err != nil {
		return SQL{}, err
	}

	// Prepare Delete SQL statement from string
	delete, err = DB.Prepare(fmt.Sprintf(deleteStr,
		tconfig.TableName, tconfig.ColID,
	))
	if err != nil {
		return SQL{}, err
	}

	// Prepare Read SQL statement from string
	read, err = DB.Prepare(fmt.Sprintf(readStr,
		tconfig.ColName, tconfig.ColData, tconfig.ColVersion, tconfig.TableName, tconfig.ColID,
	))
	if err != nil {
		return SQL{}, err
	}

	// Prepare Read Version SQL statement from string
	readVer, err = DB.Prepare(fmt.Sprintf(readVerStr,
		tconfig.ColVersion, tconfig.TableName, tconfig.ColID,
	))
	if err != nil {
		return SQL{}, err
	}

	// Prepare Get All Documents Metadata SQL statement from string
	getDocsInfo, err = DB.Prepare(fmt.Sprintf(getDocsInfoStr,
		tconfig.ColID, tconfig.ColName, tconfig.TableName,
	))
	if err != nil {
		return SQL{}, err
	}

	// Return SQL connector struct object
	return SQL{
		dsn:     dsn,
		tconfig: tconfig,
		DB:      DB,

		createStmt:      create,
		updateStmt:      update,
		readStmt:        read,
		getDocsInfoStmt: getDocsInfo,
		readVerStmt:     readVer,
		deleteStmt:      delete,
	}, nil
}

// Define function used for executing Create SQL statement
func (db *SQL) Create(doc Document) error {
	if _, err := db.createStmt.Exec(doc.Name, doc.Data, 1); err != nil {
		return errserv.ErrDBCreate
	}

	return nil
}

// Define function used for executing Update SQL statement
func (db *SQL) Update(doc Document) error {

	// Check if Document exists by using ReadVer function
	if _, err := db.ReadVer(doc.ID); err != nil {
		return errserv.ErrUnknownDocument
	}

	if _, err := db.updateStmt.Exec(doc.Data, doc.Version, doc.ID); err != nil {
		return errserv.ErrDBUpdate
	}

	return nil
}

// Define function used for executing Delete SQL statement
func (db *SQL) Delete(id int) error {

	// Check if Document exists by using ReadVer function
	if _, err := db.ReadVer(id); err != nil {
		return errserv.ErrUnknownDocument
	}

	if _, err := db.deleteStmt.Exec(id); err != nil {
		return errserv.ErrDBDelete
	}

	return nil
}

// Define function used for executing Read Document Data SQL statement
func (db *SQL) Read(id int) (Document, error) {
	var doc Document

	// Read Document data to doc variable
	if err := db.readStmt.QueryRow(id).Scan(&doc.Name, &doc.Data, &doc.Version); err != nil {
		return Document{}, errserv.ErrDBRead
	}

	// If Read returned no error, it means document ID is also correct
	doc.ID = id

	return doc, nil
}

// Define function used for executing Read Version SQL statement
func (db *SQL) ReadVer(id int) (int, error) {
	var version int

	// Read Document version to version variable
	if err := db.readVerStmt.QueryRow(id).Scan(&version); err != nil {
		return 0, errserv.ErrDBRead
	}

	return version, nil
}

// Define function used for executing Read All Documents Metadata SQL statement
func (db *SQL) GetDocInfo() (map[int]string, error) {
	retMap := make(map[int]string) // map id: name
	var id int
	var name string

	// Get all rows from getDocsInfo SQL statement
	rows, err := db.getDocsInfoStmt.Query()
	if err != nil {
		return nil, errserv.ErrDBRead
	}
	defer rows.Close()

	// For every Row returned try to create new key-value pair in retMap
	for rows.Next() {
		if err := rows.Scan(&id, &name); err != nil {
			return nil, errserv.ErrDBRead
		}
		retMap[id] = name
	}

	return retMap, nil
}
