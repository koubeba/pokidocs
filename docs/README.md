# Getting Started

### Reference Documentation

Uruchomienie aplikacji: 
1. Uruchamiamy aplikację za pomocą Intellij - Konfiguracja SpringBoot. Dodatkowo trzeba dodać do VM options:
--add-exports=javafx.graphics/com.sun.javafx.util=ALL-UNNAMED
--add-exports=javafx.base/com.sun.javafx.reflect=ALL-UNNAMED
--add-exports=javafx.graphics/com.sun.javafx.scene.layout=ALL-UNNAMED
--add-exports=javafx.graphics/com.sun.javafx.application=ALL-UNNAMED
--add-exports=javafx.graphics/com.sun.javafx.tk=ALL-UNNAMED
--add-exports=javafx.graphics/com.sun.javafx.scene.text=ALL-UNNAMED
--add-exports=javafx.base/com.sun.javafx.binding=ALL-UNNAMED
--add-exports=javafx.graphics/com.sun.javafx.scene.input=ALL-UNNAMED
--add-exports=javafx.base/com.sun.javafx.event=ALL-UNNAMED
--add-exports=javafx.graphics/com.sun.javafx.stage=ALL-UNNAMED
--add-exports=javafx.graphics/com.sun.javafx.scene=ALL-UNNAMED
--add-exports=javafx.base/com.sun.javafx.collections=ALL-UNNAMED
--add-exports=javafx.graphics/com.sun.javafx.scene.traversal=ALL-UNNAMED
--add-exports=javafx.graphics/com.sun.javafx.geom=ALL-UNNAMED
--add-exports=javafx.base/com.sun.javafx.logging=ALL-UNNAMED
--add-exports=javafx.base/com.sun.javafx=ALL-UNNAMED

Od modułowej javy 9 są jakieś problemy z manualną konfiguracją javyFX. 


Ważne jest to, aby JAVA_HOME było ustawione na jave 11, a nie na jakoś inną. 
Sprawdzenie: *java -version*