package pl.nuszel.solutions.docs.tcp.databind.request;


import com.google.gson.Gson;
import pl.nuszel.solutions.docs.tcp.databind.TransformData;
import pl.nuszel.solutions.docs.tcp.databind.TransformType;

public class MessagePreparer {

	private Gson gson = new Gson();


	public String allDocumentsMessage() {
		var msg = new Message(MessageType.ALL_DOCUMENTS, new Object());
		return addExtraSign(msg);
	}

	public String documentMessage(long documentId) {
		var msg = new Message(MessageType.REQUEST_DOC, new BlobRequest(documentId));
		return addExtraSign(msg);
	}


	public String subscribeMessage(long documentId) {
		var msg = new Message(MessageType.SUBSCRIBE, new SubscribeRequest(documentId));
		return addExtraSign(msg);
	}

	public String unsubscribeMessage() {
		var msg = new Message(MessageType.UNSUBSCRIBE, new UnsuscribeRequest());
		return addExtraSign(msg);
	}

	public String createDocumentMessage(String documentName) {
		var msg = new Message(MessageType.CREATE_DOC, new CreateDocRequest(documentName));
		return addExtraSign(msg);
	}

	public String deleteDocumentMessage(long docId) {
		var msg = new Message(MessageType.DELETE_DOC, new DeleteDocRequest(docId));
		return addExtraSign(msg);
	}

	public String transformMessage(long docId, TransformType transformType, int location, String data, long version) {
		var msg = new Message(MessageType.TRANSFORM, new TransformData(transformType.name, docId, location, data, version));
		return addExtraSign(msg);
	}

	private String addExtraSign(Message msg){
		var message = gson.toJson(msg);
		char c = 0x03;
		return message.concat(String.valueOf(c));
	}
}
