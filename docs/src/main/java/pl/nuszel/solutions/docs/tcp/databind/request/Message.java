package pl.nuszel.solutions.docs.tcp.databind.request;


class Message {
	private String typeof;

	private Object data;

	Message(MessageType messageType, Object data) {
		this.typeof = messageType.realName();
		this.data = data;
	}
}
