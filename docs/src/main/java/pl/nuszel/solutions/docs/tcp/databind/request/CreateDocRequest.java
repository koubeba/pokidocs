package pl.nuszel.solutions.docs.tcp.databind.request;

class CreateDocRequest {

	private String name;

	CreateDocRequest(String name) {
		this.name = name;
	}
}
