package pl.nuszel.solutions.docs.tcp.databind.request;

class DeleteDocRequest {

	private long document_id;

	DeleteDocRequest(long document_id) {
		this.document_id = document_id;
	}
}
