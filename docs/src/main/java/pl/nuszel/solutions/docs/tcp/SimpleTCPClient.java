package pl.nuszel.solutions.docs.tcp;

import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

final class SimpleTCPClient {

    private Socket socket;
    private PrintWriter writer;
    private BufferedReader reader;


    /**
     * inicjacja gniazda TLS oraz utworzenie writera i readera
     */
    SimpleTCPClient(String ip, int port) throws IOException {
        setupSSLEnvs();
        var factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        socket = factory.createSocket(ip, port);
        writer = new PrintWriter(socket.getOutputStream(), true);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }


    /**
     * Wysłanie wiadomości za pośrednictwem pakietów TCP/IP
     * @param msg
     * @throws IOException
     */
    void sendMessage(String msg) throws IOException {
        writer.println(msg);
    }

    /**
     * czyta ze streama
     */
    int read() throws IOException {
        return reader.read();
    }

    /**
     * Ustawienie zmiennych srodowiskowych SSL
     */
    private void setupSSLEnvs() {
        System.setProperty("javax.net.ssl.trustStore","cacerts");
        System.setProperty("javax.net.ssl.trustStorePassword","changeit");
    }

    /**
     * Zamyka otwarte streamy
     * @throws IOException
     */
    void stopConnection() throws IOException {
        writer.close();
        reader.close();
        socket.close();
    }
}

