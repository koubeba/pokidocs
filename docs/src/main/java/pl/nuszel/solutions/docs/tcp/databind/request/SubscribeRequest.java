package pl.nuszel.solutions.docs.tcp.databind.request;

class SubscribeRequest {

	private long document_id;

	SubscribeRequest(long document_id) {
		this.document_id = document_id;
	}
}
