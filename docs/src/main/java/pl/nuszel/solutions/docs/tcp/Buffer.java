package pl.nuszel.solutions.docs.tcp;

import pl.nuszel.solutions.docs.tcp.databind.TransformData;

import java.util.*;

/**
 * Bufor przechowujący historię transformacji, pozycje kursora oraz aktualną wersję dokumentu
 */
class Buffer {
	EditorController editorController;

	ArrayList <TransformData> history;

	String actualVersion;

	Caret caret;

	Long invalidatedBufferCaret = null;

	Buffer(EditorController editorController){
		this.editorController = editorController;
	}

	void initialize(String payload) {
		this.actualVersion = payload;
		this.history = new ArrayList<>();
		this.caret = new Caret(0);
	}

	void resolveTransformationAction(TransformData transformData) {
		final long version = transformData.getVersion();
		if (existsSameVersionTransformation(version)) {
			invalidateBuffer();
		} else {
			appendTransformation(transformData);
		}
	}

	private void appendTransformation(TransformData transformData) {
		history.add(transformData);
		var builder = new StringBuilder(actualVersion);

		try {
			switch (transformData.getType()) {
				case "insert":
					builder.insert(transformData.getLocation(), transformData.getData());
					if (caret.isCaretAfterTransformation(transformData)) {
						editorController.caretPosition += transformData.getData().length();
					}
					break;

				case "delete":
					if (caret.isCaretAfterTransformation(transformData)) {
						editorController.caretPosition -= Math.min(this.caret.position - transformData.getLocation(), Long.valueOf(transformData.getData()));
					}
					builder.delete(transformData.getLocation(), Integer.valueOf(transformData.getData()));
					break;
			}
			this.actualVersion = builder.toString();
		}catch (Exception e) {
			editorController.getActualDocumentVersion();
		}
	}


	private boolean existsSameVersionTransformation(long version) {
		return history.stream().anyMatch(transform -> transform.getVersion() == version);
	}

	private void invalidateBuffer() {
		editorController.getActualDocumentVersion();
	}

	class Caret {

		long position;

		boolean isCaretAfterTransformation(TransformData transformData){
			return position > transformData.getLocation();
		}

		Caret(long position) {
			this.position = position;
		}
	}
}
