package pl.nuszel.solutions.docs.tcp.databind.response;

public enum  ResponseType {
	DOCUMENT_LIST("docsinfo"),
	DOCUMENT("blob"),
	ERROR("error"),
	TRANSFORM("transform"),
	NONE("");
	String name;

	ResponseType(String name) {
		this.name = name;
	}

	public static ResponseType getByName(String name) {
		for(ResponseType responseType : values()) {
			if (responseType.name.equals(name)) {
				return responseType;
			}
		}
		return null;
	}
}
