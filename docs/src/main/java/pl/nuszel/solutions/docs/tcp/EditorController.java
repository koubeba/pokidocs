package pl.nuszel.solutions.docs.tcp;

import javafx.beans.value.ChangeListener;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import pl.nuszel.solutions.docs.tcp.databind.TransformType;

import java.util.ArrayList;
import java.util.List;


@Controller
public class EditorController {

    @Autowired
    private NetworkService networkService;

    @FXML
    TextArea editorText;

    @FXML
    ListView <String> files;

    List<String> fileList = new ArrayList<>();

    @FXML
    TextField fileName;

    @FXML
    TextField error;

    private long currentDocId;

    long currentDocVersion;

    Buffer buffer = new Buffer(this);

    private LocalBuffer localBuffer = new LocalBuffer();

    private Integer index = 0;

    int caretPosition;

    ChangeListener<? super String> listener = (observable, oldValue, newValue) -> {
        int index = StringUtils.indexOfDifference(oldValue, newValue);
        this.index = index;
        var difference = StringUtils.difference(oldValue, newValue);
        // insert
        if (oldValue.length() < newValue.length()) {
            localBuffer.addNewTransformation(difference.substring(0, newValue.length() - oldValue.length()));
        } else {
            localBuffer.incrementDeletion(oldValue.length() - newValue.length());
        }
        caretPosition = editorText.getCaretPosition();
    };
    private boolean connected = false;

    public void connect() {
        editorText.textProperty().addListener(listener);
        networkService.establishConnection();
        networkService.getAllDocuments();
        connected = true;
    }

    public void documentSelected(Event event) {
        var name = files.getSelectionModel().getSelectedItem();
        long docId = Long.valueOf(name.substring(0, name.indexOf('-')));
        if (currentDocId != 0 && docId != currentDocId) {
            networkService.unsubscribeDocument();
        }
        networkService.subscribeDocument(docId);
        currentDocId = docId;
        networkService.getDocument(currentDocId);
    }

    @Scheduled(fixedRate = 10000)
    public void updateDocumentList(){
        if (connected) {
            networkService.getAllDocuments();
        }
    }

    public void addDocument() {
        if(!fileName.getText().isBlank()) {
            try {
                networkService.createDocument(fileName.getText());
                networkService.getAllDocuments();
            } catch (Exception e) {

            }
        }
    }

    public void removeDocument() {
        var name = files.getSelectionModel().getSelectedItem();
        long docId = Long.valueOf(name.substring(0, name.indexOf('-')));
        if (docId > 0) {
            networkService.deleteDocument(docId);
            networkService.getAllDocuments();
        }
    }

    void updateEditorText() {
        editorText.setText(buffer.actualVersion);
        if (caretPosition < editorText.getText().length() && editorText.getText().length() > 0) {
            System.out.println(caretPosition);
            editorText.positionCaret(caretPosition);
        } else {
            editorText.positionCaret(editorText.getText().length());
        }
    }


    @Scheduled(fixedRate = 250)
    void sendLocalBuffer() {
        if (localBuffer.currentBufferType == null) {
            return;
        } else if(localBuffer.currentBufferType.equals(TransformType.INSERT)){
            var singleTransformation = localBuffer.groupTransformations();
            var indexToSend = index;
            if(singleTransformation.length() > 1) {
                indexToSend -= singleTransformation.length() - 1;
            }
            networkService.insert(currentDocId, indexToSend , singleTransformation , ++currentDocVersion);
        } else {
            int deletion = (int)localBuffer.getDeletion();
            networkService.delete(currentDocId, index, String.valueOf(deletion), ++currentDocVersion);
        }
    }

    @Scheduled(fixedRate = 3000)
    void getActualDocumentVersion() {
        if (currentDocId > 0) {
            networkService.getDocument(currentDocId);
        }
    }

    @Scheduled(fixedRate = 100)
    void updateIndex() {
        try {
            caretPosition = editorText.getCaretPosition();
        } catch (Exception e) {

        }
    }
}
