package pl.nuszel.solutions.docs.tcp.databind.request;

public enum MessageType {
	ALL_DOCUMENTS("requestdocsinfo"),
	TRANSFORM("transform"),
	REQUEST_DOC("requestblob"),
	SUBSCRIBE("subscribe"),
	UNSUBSCRIBE("unsubscribe"),
	CREATE_DOC("createdoc"),
	DELETE_DOC("deletedoc");

	private String name;

	MessageType(String name) {
		this.name = name;
	}

	public String realName() {
		return name;
	}
}
