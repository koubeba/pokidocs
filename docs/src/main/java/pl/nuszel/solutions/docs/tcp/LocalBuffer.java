package pl.nuszel.solutions.docs.tcp;

import pl.nuszel.solutions.docs.tcp.databind.TransformType;

import java.util.ArrayList;
import java.util.List;

class LocalBuffer {
	List<String> insertions = new ArrayList<>();
	long delete = 0;
	TransformType currentBufferType;

	String groupTransformations() {
		var builder = new StringBuilder();
		for(var change : insertions) {
			builder.append(change);
		}
		clear();
		return builder.toString();
	}

	long getDeletion() {
		var del = delete;
		clear();
		return del;
	}

	void addNewTransformation(String text) {
		if (insertions.size() == 0) {
			currentBufferType = TransformType.INSERT;
		}
		insertions.add(text);
	}

	void incrementDeletion(long numberOfDeletedCharacters) {
		if (delete == 0) {
			currentBufferType = TransformType.DELETE;
		}
		delete += numberOfDeletedCharacters;
	}

	void clear(){
		insertions.clear();
		delete = 0;
		currentBufferType = null;
	}
}

