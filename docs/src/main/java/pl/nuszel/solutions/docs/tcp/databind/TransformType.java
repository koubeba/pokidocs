package pl.nuszel.solutions.docs.tcp.databind;

public enum TransformType {
	INSERT("insert"), DELETE("delete");

	public String name;

	TransformType(String name) {
		this.name = name;
	}
}
