package pl.nuszel.solutions.docs.tcp.databind;

public class TransformData {
	private String type;
	private long document_id;
	private int location;
	private String data;
	private long version;

	public TransformData(String type, long document_id, int location, String data, long version) {
		this.type = type;
		this.document_id = document_id;
		this.location = location;
		this.data = data;
		this.version = version;
	}

	public String getType() {
		return type;
	}

	public long getDocument_id() {
		return document_id;
	}

	public int getLocation() {
		return location;
	}

	public String getData() {
		return data;
	}

	public long getVersion() {
		return version;
	}
}
