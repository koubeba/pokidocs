package pl.nuszel.solutions.docs.tcp;

import com.google.gson.Gson;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.nuszel.solutions.docs.tcp.databind.response.*;

import java.util.stream.Collectors;

@Component
class ActionReader {

	@Autowired
	EditorController editorController;

	private Gson gson = new Gson();


	/**
	 * Odczytanie rodzaju wiadomości
	 */
	void doActionAfterResponse(String json) {
		Platform.runLater(() -> {
			switch (resolveTypeOfMessage(json)) {
				case DOCUMENT_LIST:
					documentListAction(json);
					break;
				case DOCUMENT:
					documentAction(json);
					break;

				case TRANSFORM:
					transformAction(json);
					break;

				case ERROR:
					errorAction(json);
					break;

				case NONE:
					break;
			}
		});
	}

	/**
	 * Odczytanie wiadomosci typu Error i zalogowanie go.
	 *
	 * @param json
	 */

	private void errorAction(String json) {
		var resp = gson.fromJson(json, ErrorResponse.class);
		var data = resp.data;
		System.err.println("ERROR: " + data.message);

		if (resp.data.message.equals("Connection Closed")) {
			editorController.error.setText("Connection closed!!!");
		}
	}

	/**
	 * Odczytanie transformacji i uaktalnienie listy zmian
	 */
	private void transformAction(String json) {
		var resp = gson.fromJson(json, TransformResponse.class);
		var transformData = resp.getData();
		editorController.editorText.textProperty().removeListener(editorController.listener);
		editorController.buffer.resolveTransformationAction(transformData);
		editorController.currentDocVersion = transformData.getVersion();
		editorController.updateEditorText();
		editorController.editorText.textProperty().addListener(editorController.listener);
	}


	/**
	 * Akcje zwiazane z otrzymaniem pakietu zawierajacego cały plik
	 *
	 * @param json
	 */
	private void documentAction(String json) {
		System.out.println("DOCUMENT ACTION");
		var respDoc = gson.fromJson(json, DocumentResponse.class);
		editorController.editorText.textProperty().removeListener(editorController.listener);
		editorController.buffer.initialize(respDoc.data.payload);
		editorController.currentDocVersion = respDoc.data.version;
		editorController.updateEditorText();
		editorController.editorText.textProperty().addListener(editorController.listener);
	}

	/**
	 * Akcje związane z otrzymaniem
	 *
	 * @param json
	 */
	private void documentListAction(String json) {
		var respList = gson.fromJson(json, DocumentListResponse.class);
		var files = respList.data.docs_info;
		var observableList = files.entrySet().stream().map(keyValue -> keyValue.getKey() + "-" + keyValue.getValue()).collect(Collectors.toList());
		if (!observableList.equals(editorController.fileList)) {
			editorController.fileList = observableList;
			editorController.files.setItems(FXCollections.observableArrayList(observableList));
		}
	}

	/**
	 * Odczytanie rodzaju nadchodzacej wiadomosci
	 *
	 * @param json
	 * @return
	 */
	private ResponseType resolveTypeOfMessage(String json) {
		if (json.equals("{}")) {
			return ResponseType.NONE;
		}
		var response = gson.fromJson(json, Response.class);
		return ResponseType.getByName(response.typeof);
	}
}
