package pl.nuszel.solutions.docs.tcp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.nuszel.solutions.docs.tcp.databind.TransformType;
import pl.nuszel.solutions.docs.tcp.databind.request.MessagePreparer;


@Service
class NetworkService {

	@Autowired
	private TcpRunner tcpRunner;

	private MessagePreparer messagePreparer = new MessagePreparer();

	void establishConnection() {
		tcpRunner.init();
	}

	/**
	 * Pobranie wszystkich dokumentow
	 */
	void getAllDocuments() {
		String msg = messagePreparer.allDocumentsMessage();
		tcpRunner.sendMessage(msg);
	}

	/**
	 * Pobranie tresci wybranego dokumentu
	 * @param documentId
	 */
	void getDocument(long documentId) {
		String msg = messagePreparer.documentMessage(documentId);
		tcpRunner.sendMessage(msg);
	}

	/**
	 * Subskypcja dokumnetu
	 * @param documentId
	 */
	void subscribeDocument(long documentId) {
		String msg = messagePreparer.subscribeMessage(documentId);
		tcpRunner.sendMessage(msg);
	}

	/**
	 * Odsubskrybowanie dokumentu
	 */
	void unsubscribeDocument() {
		String msg = messagePreparer.unsubscribeMessage();
		tcpRunner.sendMessage(msg);
	}

	/**
	 * Utworzenie dokumentu o wybranej nazwie
	 * @param documentName
	 */
	void createDocument(String documentName) {
		String msg = messagePreparer.createDocumentMessage(documentName);
		tcpRunner.sendMessage(msg);
	}

	/**
	 * Usuniecie dokumentu o wybranym ID
	 * @param docId
	 */
	void deleteDocument(long docId) {
		String msg = messagePreparer.deleteDocumentMessage(docId);
		tcpRunner.sendMessage(msg);
	}

	/**
	 * Transformacja z dodaniem treści
	 * @param docId
	 * @param location
	 * @param data
	 * @param version
	 */
	void insert(long docId, int location, String data, long version) {
		String msg = messagePreparer.transformMessage(docId, TransformType.INSERT, location, data, version);
		tcpRunner.sendMessage(msg);
	}

	/**
	 * Transformacja z usunieciem tresci
	 * @param docId
	 * @param location
	 * @param data
	 * @param version
	 */
	void delete(long docId, int location, String data, long version) {
		String msg = messagePreparer.transformMessage(docId, TransformType.DELETE, location, data, version);
		tcpRunner.sendMessage(msg);
	}
}