package pl.nuszel.solutions.docs.tcp.databind.response;

import pl.nuszel.solutions.docs.tcp.databind.TransformData;

public class TransformResponse {
	private String typeof;
	private TransformData data;

	public String getTypeof() {
		return typeof;
	}

	public void setTypeof(String typeof) {
		this.typeof = typeof;
	}

	public TransformData getData() {
		return data;
	}

	public void setData(TransformData data) {
		this.data = data;
	}
}
