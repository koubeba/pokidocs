package pl.nuszel.solutions.docs.tcp.databind.response;

public class DocumentData {
	public long document_id;
	public long full_size;
	public String name;
	public String payload;
	public long version;
}
