package pl.nuszel.solutions.docs.tcp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
class TcpRunner {

	@Autowired
	private ActionReader actionReader;

	@Value("${dominik.server.ip}")
	private String serverIp;

	@Value("${dominik.server.port}")
	private int serverPort;

	private SimpleTCPClient simpleTCPClient;

	/**
	 * Uruchomienie wątku czytającego wiadomości
	 */
	void init() {
		try {
			simpleTCPClient =  new SimpleTCPClient(serverIp, serverPort);
		}catch (IOException e) {
			e.printStackTrace();
			System.out.println("Can't connect to server ... ");
			System.out.println("Closing app...");
			System.exit(1);
		}
		launchReader();
	}

	/**
	 * Wysyłka wiadomości poprzez gniazdo TCP
	 */

	void sendMessage(String msg){
		try {
			simpleTCPClient.sendMessage(msg);
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	/**
	 * Uruchamia wątek czytający
	 */
	private void launchReader() {
		new Thread(() -> {
			int c;
			try {
				while (true){
					var builder = new StringBuilder();
					while ((c = simpleTCPClient.read()) != 0x03) {
						builder.append((char)c);
					}
					actionReader.doActionAfterResponse(builder.toString());
				}
			} catch (IOException e) {
				System.out.println("Error when reading");
				e.printStackTrace();
			}
		}).start();
	}
}
