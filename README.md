# Pokidocs

Projekt na przedmiot TIN
Kolaboratywny edytor dokumentów

## Aplikacje

- [Serwer napisany w Go](server)
- [Klient napisany w Python](python_app)
- [Klient napisany w Java](fxClient/src)
- [Klient Webowy napisany w Node JS](web)