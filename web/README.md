# Aplikacja webowa

## Technologie

* Node.js 10
* Vue 2.6
* npm

## Użyte biblioteki

### Komunikacja sieciowa

* **ws**: obsługa WebSocketów dla node.js (Wymagane dla stworzenia serwera webowego komunikującego się z aplikacją)


### Inne

* **nodemon**: uruchamianie serwera w tle
* **babel**: umożliwienie korzystania z ES

## Sposób działania

### Uruchomienie

Aplikacja podzielona jest na klienta i proxy.

#### Klient

Klient znajduje się w folderze /pokidocs

Aplikacja bazuje na Node.js i npm, dlatego po ściągnięciu jej z repozytorium wymagane jest zainstalowanie zależności (biblioteki nodemon i babel są wymagane):

`npm install`

Aby uruchomić aplikację frontendową należy wpisać komendę:

`npm run serve`

#### Webproxy

Pliki znajdują się w folderze /webproxy

Podobnie jak w przypadku klienta, zależności można zainstalować komendą:

`npm install`

Skrypt uruchamiający klienta TCP i serwer webowy umieszczone są tymczasowo w pliku test.js.
Skrypt uruchamiany jest poleceniem:

`npm start`

Proxy działa pod adresem **localhost:3210**
Aplikacja będzie dostępna pod adresem **localhost:8080**.

### Klient TCP

Klient komunikuje się z głównym serwerem przez sockety TCP. Przy połączeniu wysyła mu komunikat powitalny korzystając z komendy 
>  !greet

Po otrzymaniu wiadomości z serwera, klient komunikuje się za pomocą obiektu EventEmitter z serwerem webowym i przesyła mu treść odebranej wiadomości. Serwer webowy wysyła ją następnie swoim klientom webowym.
Po otrzymaniu wiadomości z serwera webowego, wiadomość wysyłana jest do serwera głównego. Klient działa więc jak proxy między websocketami a socketami.

### Serwer webowy

Do serwera webowego, opartego o websockety, będą łączyć się klienci, będący aplikacjami sails.js.