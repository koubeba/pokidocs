// A TCP based client

const net = require('net');
const buf = require('buffer');
const logger = require('./logger');

import { Messenger } from './message_event'

const timeout = 5000;

export class Client {
  constructor(host, port, token, messenger = null, keepAlive = false) {
    this.host = host;
    this.port = port;
    this.token = token;
    this.keepAlive = keepAlive;
    this.messenger = messenger;
    this.socket = new net.Socket();
    this.socket.setEncoding('utf-8');
  }

  connect(webSocket) {
    let that = this;

    this.socket.connect(this.port, this.host, function(connection) {
      logger.Log(logger.LogType.CONNECTED, that.host, that.port);
    });

    if (this.keepAlive) {
      this.socket.setKeepAlive(true, timeout);
    } else {
      this.socket.setTimeout(timeout);
    }

    this.socket.on('data', function(data) {
      logger.Log(logger.LogType.DATA, that.host, that.port, data);

      let parsedJSON = that.parseJSON(data);
      webSocket.send(parsedJSON);
    });

    this.socket.on('error', function() {
      logger.Log(logger.LogType.ERROR, that.host, that.port);
    });

    this.socket.on('timeout', function() {
      logger.Log(logger.LogType.TIMEOUT, that.host, that.port);
      that.socket.end();
    });

    this.socket.on('close', function() {
      logger.Log(logger.LogType.CLOSE, that.host, that.port);

      // Message webserver to be closed
      that.messenger.emitClose(that.token);
    });

    process.on('SIGINT', function() {
      that.socket.end();
    });

  }

  close() {
    this.socket.destroy()
  }

  sendDataToServer(data) {
    this.socket.write(this.createJsonBuffer(data));
    logger.Log(logger.LogType.SENT, this.host, data);
  }

  createJsonBuffer(json) {
    return buf.Buffer.concat([
        buf.Buffer.from(json),
        buf.Buffer.from([0x03])
    ]);
  }

  parseJSON(json) {
    return json.substr(0, json.length-1)
  }
}
