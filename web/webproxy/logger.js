export const LogType = Object.freeze({
    CONNECTED: "[CONNECTED] Connected to ",
    TOKEN_ASSIGN: "[TOKEN ASSIGN] Assigned token for a new websocket: ",
    ERROR: "[ERROR] Error occurred while connected to ",
    DISCONNECTED: "[DISCONNECTED] Disconnected from ",
    DATA: "[RECEIVED] Received data from ",
    TIMEOUT: "[TIMEOUT] Timeout while connected to ",
    CLOSE: "[CLOSED] Connection closed to ",
    CLOSE_SERVER: "[CLOSED] Closed server ",
    SENT: "[SENT] Sent data to server ",
    INVALID_JSON: "[ERROR] Error while receiving from ",
    NO_DOCUMENT: "[ERROR] Document with ID doesn't exist ",
    DOCUMENT_ERROR: "[ERROR] Error while connecting to document with ID ",
    DOCUMENT_ID_MISMATCH: "[ERROR] Received data for unknown document with ID "
});

export function Log(type, host = null, port = null, msg = null) {
    var data = type.toString();

    if (host != null && port != null) {
        data += host + ":" + port + " ";
    }

    if (msg != null) {
        data += "Message: " + msg;
    }

    console.log(data);
}

export function LogDocumentError(type, id) {
    console.log(type.toString() + id.toString());
}