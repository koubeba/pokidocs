// A webserver based on WebSockets

const WebSocket = require('ws');
const logger = require('./logger');
const uuidv1 = require('uuid/v1');

import { Messenger } from './message_event'

import { Client } from './client'
import { TLSClient } from './tlsclient'

export class Webserver {

    constructor(host, port, serverHost, serverPort, encrypted = true) {
        this.host = host;
        this.port = port;
        this.serverHost = serverHost;
        this.serverPort = serverPort;
        this.encrypted = encrypted;

        // Create a token-websocket chart
        this.websockets = {};
        this.clients = {};

        this.serv = new WebSocket.Server({
            port: this.port
        });

        this.messenger = new Messenger();

        if (this.messenger != null) {
            // this.messenger.addMessageSendFun(this.sendMessageToSocket.bind(this));
            this.messenger.addMessageClose(this.close.bind(this));
        } 
    }

    accept() {
        let that = this;

        if (this.encrypted) 
            console.log('Starting the server... Encrypted')
        else
            console.log('Starting the server... No encryption')

        this.serv.on('connection', function(webSocket, request) {
            logger.Log(logger.LogType.CONNECTED, request.headers['host'], request.connection.remoteAddress);

            // Assign a new tocket for the websocket
            var token = uuidv1();
            that.websockets[token] = webSocket;

            logger.Log(logger.LogType.TOKEN_ASSIGN, request.headers['host'], request.connection.remoteAddress, token);

            // Create a client for a newly connectbaed websocket
            ///if (this.encrypted)
                that.clients[token] = new TLSClient(that.serverHost, that.serverPort, token, that.messenger, true);
            //else
            //    that.clients[token] = new Client(that.serverHost, that.serverPort, token, that.messenger, true);
            // Connect the client
            that.clients[token].connect(webSocket);

            webSocket.on('message', function(message) {
                logger.Log(logger.LogType.DATA, 'web', 'socket;token: ' + token, message);

                that.clients[token].sendDataToServer(message);

            });

            webSocket.on('close', function() {
                console.log(logger.LogType.DISCONNECTED + ' from ' + token)
                delete that.websockets[token]
                that.clients[token].close()
                delete that.clients[token]
            })
        });
    }

    shutdown() {

        for (var key in this.websockets) {
            this.websockets[key].close()
        }

        for (var key in this.clients) {
            this.clients[key].close()
        }

        this.serv.close()
    }

    close(token) {
        // The connection from client was closed: try to reconnect...
        if (this.encrypted)
            this.clients[token] = new TLSClient(this.serverHost, this.serverPort, token, this.messenger, true);
        else
            this.clients[token] = new Client(this.serverHost, this.serverPort, token, this.messenger, true);
    }

    sendMessageToSocket(message, token) {
        logger.Log(logger.LogType.DATA, 'socket:', 'client;token: ' + token, message);
        this.websockets[token].send(message);
    }
}
