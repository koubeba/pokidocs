// A TCP based client

const tls = require('tls');
const net = require('net');
const buf = require('buffer');
const logger = require('./logger');
const fs = require('fs')

import { Messenger } from './message_event'

const timeout = 5000;

export class TLSClient {
  constructor(host, port, token, messenger = null, keepAlive = false) {
    this.host = host;
    this.port = port;
    this.token = token;
    this.keepAlive = keepAlive;
    this.messenger = messenger;
    this.socket = null
  }

    connect(webSocket) {
      let that = this;
      const options = {
        // The client obtains certificate from the server, thereore
        // the connection is unauthorized at first
        rejectUnauthorized: false
      };
    
      var socket = tls.connect(this.port, this.host, options, () => {
        console.log(logger.LogType.CONNECTED, that.host, that.port)
        process.stdin.pipe(socket);
        process.stdin.resume();
    })

    this.socket = socket

    this.socket.on('error', function() {
      logger.Log(logger.LogType.ERROR, that.host, that.port);
    });

    this.socket.on('data', function(data) {
      logger.Log(logger.LogType.DATA, that.host, that.port, data);

      let parsedJSON = that.parseJSON(data);
      webSocket.send(parsedJSON);
    });

    this.socket.on('timeout', function() {
      logger.Log(logger.LogType.TIMEOUT, that.host, that.port);
      that.socket.end();
    });

    this.socket.on('close', function() {
      logger.Log(logger.LogType.CLOSE, that.host, that.port);

      // Message webserver to be closed
      that.messenger.emitClose(that.token);
    });

    process.on('SIGINT', function() { 
      that.socket.end();
    });

  }

  close() {
    this.socket.end()
  }

  sendDataToServer(data) {
    this.socket.write(this.createJsonBuffer(data));
    logger.Log(logger.LogType.SENT, this.host, data);
  }

  createJsonBuffer(json) {
    return buf.Buffer.concat([
        buf.Buffer.from(json),
        buf.Buffer.from([0x03])
    ]);
  }

  parseJSON(json) {
    return json.toString().substr(0, json.length-1)
  }
}
