const events = require('events');

export const receivedEvent = 'received';
export const sendEvent = 'send';
export const closeEvent = 'clientClosed';

export class Messenger {

    constructor() {
        this.emitter = new events.EventEmitter();
    }

    addMessageReceivedFun(fun) {
        this.emitter.addListener(receivedEvent, fun);
    }

    addMessageSendFun(fun) {
        this.emitter.addListener(sendEvent, fun);
    }

    addMessageClose(fun) {
        this.emitter.addListener(closeEvent, fun);
    }

    emitMessageReceived(data) {
        this.emitter.emit(receivedEvent, data);
    }

    emitMessageToSend(data) {
        this.emitter.emit(sendEvent, data);
    }

    emitClose() {
        this.emitter.emit(closeEvent);
    }
}
