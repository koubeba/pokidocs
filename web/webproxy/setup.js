import {Client} from './client';
import {Webserver} from "./webserver";
import {Messenger} from "./message_event";
import { exists } from 'fs';
const tls = require('tls')
const fs = require('fs')
const uuidv1 = require('uuid/v1');
const args = require('minimist')(process.argv.slice(2));

if (args['_'].length != 3 && args['_'].length != 2) {
    console.log('Usage: --HOST --PORT --ENCRYPTED [True or False]')
    process.exit()
}

if (args['_'][2] == 'False')
    var webserver = new Webserver('localhost', '3210', args['_'][0], args['_'][1], false);
else
    var webserver = new Webserver('localhost', '3210', args['_'][0], args['_'][1]);

webserver.accept();

process.on('SIGTERM', () => {
    shutdown()
});

process.on('SIGINT', () => {
    shutdown()
});

function shutdown() {
    console.log('Closing server.');
    webserver.shutdown()
}