import { mount, createLocalVue } from '@vue/test-utils'
import Edit from '../../src/components/Edit.vue'
import store from '../../src/store'
import Subscriber from '../../src/plugins/Subscriber'
import { WebSocket } from 'mock-socket'
import '../../src/plugins/vuetify'
import VueToast from 'vue-toast-notification'
import { expect } from 'chai'
import { Insertion, Deletion } from '../../src/document/transformHelper'

describe('Sorting transform buffer', () => {

  var localVue;
  var wrapper;

  before(function() {

    localVue = setupLocalVue()
    wrapper = setupWrapper(localVue)

  })

  afterEach(() => {
    resetWrapperText(wrapper)
    wrapper.vm.bufferedTransformations = []
  })

  it('sort transforms with different lengths', () => {
    wrapper.vm.bufferedTransformations = [
      new Insertion(0, 'miś', 3),
      new Insertion(0, 'kotek', 2),
      new Insertion(0, 'krem', 2),
      new Insertion(0, 'karykatura', 2)
    ]

    wrapper.vm.sortTransformBuffer().then(() => {
      expect(wrapper.vm.bufferedTransformations[0].data).to.equal('krem')
      expect(wrapper.vm.bufferedTransformations[1].data).to.equal('kotek')
      expect(wrapper.vm.bufferedTransformations[2].data).to.equal('karykatura')
      expect(wrapper.vm.bufferedTransformations[3].data).to.equal('miś')
    })
  })

  it('sort transforms with same lengths', () => {
    wrapper.vm.bufferedTransformations = [
      new Insertion(0, 'beza', 3),
      new Insertion(0, 'piesek', 2),
      new Insertion(0, 'ananas', 2),
      new Insertion(0, 'świnka', 2)
    ]

    wrapper.vm.sortTransformBuffer().then(() => {
      expect(wrapper.vm.bufferedTransformations[0].data).to.equal('ananas')
      expect(wrapper.vm.bufferedTransformations[1].data).to.equal('piesek')
      expect(wrapper.vm.bufferedTransformations[2].data).to.equal('świnka')
      expect(wrapper.vm.bufferedTransformations[3].data).to.equal('beza')
    })
  })
})

describe('Handling transform buffer', () => {

  var localVue;
  var wrapper;

  before(function() {

    localVue = setupLocalVue()
    wrapper = setupWrapper(localVue)

  })

  afterEach(() => {
    resetWrapperText(wrapper)
    wrapper.vm.bufferedTransformations = []
  })

  it('handle transforms with different lengths', async () => {
    wrapper.vm.bufferedTransformations = [
      new Insertion(0, 'miś', 3),
      new Insertion(0, 'kotek', 2),
      new Insertion(0, 'krem', 2),
      new Insertion(0, 'karykatura', 2)
    ]

    await wrapper.vm.handleTransformBuffer()
    expect(wrapper.vm.bufferedTransformations[0].data).to.equal('krem')
    expect(wrapper.vm.bufferedTransformations[1].data).to.equal('kotek')
    expect(wrapper.vm.bufferedTransformations[2].data).to.equal('karykatura')
    expect(wrapper.vm.bufferedTransformations[3].data).to.equal('miś')
  
    // Correct locations
    expect(wrapper.vm.bufferedTransformations[0].location).to.equal(0)
    expect(wrapper.vm.bufferedTransformations[1].location).to.equal(0+4)
    expect(wrapper.vm.bufferedTransformations[2].location).to.equal(0+4+5)
    expect(wrapper.vm.bufferedTransformations[3].location).to.equal(0+4+5+10)
  })
})

function setupLocalVue() {
  var localVue = createLocalVue()
  localVue.use(Subscriber)
  localVue.use(VueToast)
  return localVue;
}

function setupWrapper(localVue) {
  const $route = {
      path: '/',
      params: { id: '1' },
  }

  const $socket = new WebSocket('ws://localhost:3210')

  var wrapper = mount(Edit, {
      mocks: {
          $route,
          $socket
      },
      store,
      sockets: new WebSocket('ws://localhost:3210'),
      localVue,
      data: {
          docID: 1,
          docText: 'plecha',
          prevDocText: '',
          version: 0,
          info: '',
          bufferedTransformations: [],
          bufferState: null
    }
  })

  return wrapper;
}

function resetWrapperText(wrapper) {
  wrapper.vm.docText = ''
  wrapper.vm.version = 1
}