// mutation-types.js
const SOCKET_ONOPEN = '✅ Socket connected!'
const SOCKET_ONCLOSE = '❌ Socket disconnected!'
const SOCKET_ONERROR = '❌ Socket Error!!!'
const SOCKET_ONMESSAGE = 'Websocket message received'
const SOCKET_RECONNECT = 'Websocket reconnected'
const SOCKET_RECONNECT_ERROR = 'Websocket is having issues reconnecting..'
const TEXT_UNCHANGED = 'Unchanged'
const TEXT_CHANGED = 'Changed'
const TEXT_BUFFERED = 'Buffered'
const TEXT_APPLYING = 'Applying'

export {
  SOCKET_ONOPEN,
  SOCKET_ONCLOSE,
  SOCKET_ONERROR,
  SOCKET_ONMESSAGE,
  SOCKET_RECONNECT,
  SOCKET_RECONNECT_ERROR,
  TEXT_UNCHANGED,
  TEXT_CHANGED,
  TEXT_BUFFERED,
  TEXT_APPLYING
}