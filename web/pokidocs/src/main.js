import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import Subscriber from './plugins/Subscriber'
import VueNativeSock from 'vue-native-websocket'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/index.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEdit, faTrash, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VeeValidate from 'vee-validate';
import { ValidationProvider } from 'vee-validate';

import {
  SOCKET_ONOPEN,
  SOCKET_ONCLOSE,
  SOCKET_ONERROR,
  SOCKET_ONMESSAGE,
  SOCKET_RECONNECT,
  SOCKET_RECONNECT_ERROR
} from './mutation-types'

const mutations = {
  SOCKET_ONOPEN,
  SOCKET_ONCLOSE,
  SOCKET_ONERROR,
  SOCKET_ONMESSAGE,
  SOCKET_RECONNECT,
  SOCKET_RECONNECT_ERROR
}

Vue.use(VueNativeSock, 'ws://localhost:3210', {
  reconnection: true,
  store: store,
  mutations: mutations
})

Vue.config.productionTip = false

library.add(faEdit)
library.add(faTrash)
library.add(faPlus)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(Subscriber)
Vue.use(VueToast)
Vue.use(VeeValidate)

Vue.component('ValidationProvider', ValidationProvider);

new Vue({
  router,
  store,
  render: h => h(App),
  beforeDestroy: () => {
    this.$disconnect()
  }
}).$mount('#app')