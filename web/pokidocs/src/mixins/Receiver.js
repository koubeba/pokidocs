import * as JSONHelper from '../document/jsonHelper'

var Receiver = {
    data: function() {
        return {
            response: null
        }
    },
    created: function () {
        this.$store.watch(this.$store.getters.getConnectionState, state => {
            if (state) {
                this.$toast.open('Connected')
            } else {
                this.$toast.open({
                    message: 'Connection Lost',
                    type: 'error'
                })
            }
        })

        this.$options.sockets.onmessage = function (data) {
            this.response = JSONHelper.parseServerData(data)
            if (typeof this.response !== 'ServerError') {
                //this.$toast.open('Success: ' + this.response.dataType)
            } else {
                var code = this.response.code
                var message = this.response.message
                this.$toast.open({
                    message: code + ": " + message,
                    type: 'error'
                })
            }
        }
    },
    methods: {
        noError: function() {
            return (typeof this.response !== "null" && typeof this.response !== "undefined" && typeof this.response !== "ServerError")
        }
    }
}

export default Receiver