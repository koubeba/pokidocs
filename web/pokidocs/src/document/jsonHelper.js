import { Transformation, Insertion, Deletion } from './transformHelper'

// JSONs to send

export function requestJSON (type, data = {}) {
  return JSON.stringify({
    typeof: type,
    data: data
  })
}

export function requestDocsInfoJSON () {
  return requestJSON('requestdocsinfo');
}

export function subscribeToDocJSON (docID) {
  return requestJSON('subscribe', {
    document_id: docID
  })
}

export function unsubscribeJSON () {
  return requestJSON('unsubscribe');
}

export function requestBlobJSON (docID) {
  return requestJSON('requestblob', {
    document_id: docID
  })
}

export function createDocJSON (name) {
  return requestJSON('createdoc', {
    name: name
  })
}

export function deleteDocJSON (docID) {
  return requestJSON('deletedoc', {
    document_id: docID
  })
}

export function insertTransformJSON (docID, location, data, version) {
  return requestJSON('transform', {
    type: 'insert',
    document_id: docID,
    location: location,
    data: data,
    version: version
  })
}

export function insertionJSON (docID, insertion) {
  return requestJSON('transform', {
    type: 'insert',
    document_id: docID,
    location: insertion.location,
    data: insertion.data,
    version: insertion.version
  })
}

export function deleteTransformJSON (docID, location, data, version) {
  return requestJSON('transform', {
    type: 'delete',
    document_id: docID,
    location: location,
    data: data,
    version: version
  })
}

export function deletionJSON (docID, deletion) {
  return requestJSON('transform', {
    type: 'delete',
    document_id: docID,
    location: deletion.location,
    data: deletion.dataLength,
    version: deletion.version
  })
}

// Received data classes

export class ServerData {
  constructor (dataType) {
    this.dataType = dataType;
  }
}

export class ServerError extends ServerData {
  constructor (code, message) {
    super('error');
    this.code = code;
    this.message = message;
  }
}

export class ServerBlob extends ServerData {
  constructor (docID, fullSize, name, payload, version) {
    super('blob');
    this.docID = docID;
    this.fullSize = fullSize;
    this.name = name;
    this.payload = payload;
    this.version = version;
  }
}

export class ServerTransform extends ServerData {
  constructor (type, docID, location, data, version) {
    super('transform');
    this.type = type;
    this.docID = docID;
    this.location = location;
    this.data = data;
    this.version = version;
  }
}

export class ServerDoc extends ServerData {
  constructor (docID, name) {
    super('docinfo');
    this.docID = docID;
    this.docName = name;
  }
}

export class ServerDocs extends ServerData {
  constructor () {
    super('docsinfo');
    this.docs = [];
  }

  addDoc (doc) {
    this.docs.push(doc);
  }
}

// Receiver JSON parser

const regex = /{[\s\S]*}/g;

export function parseServerData (event) {
  let parsedData = JSON.parse(event.data.match(regex));
  let receivedData = parsedData.data

  switch (parsedData.typeof) {
    case 'error':
      return new ServerError(receivedData.code, receivedData.messWage);
    case 'transform':
      return new ServerTransform(receivedData.type, receivedData.document_id,
        receivedData.location, receivedData.data,
        receivedData.version);
    case 'blob':
      return new ServerBlob(receivedData.document_id, receivedData.full_size,
        receivedData.name, receivedData.payload, receivedData.version);
    case 'docsinfo':
      var docs = receivedData.docs_info;
      var docsInfo = new ServerDocs();
      Object.keys(docs).forEach(function(key) {
        docsInfo.addDoc(new ServerDoc(parseInt(key), docs[key]));
      });
      return docsInfo;
  }
}
