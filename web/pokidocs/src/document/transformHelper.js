var Diff = require('text-diff')

export class Transformation {
    
    constructor(type, location, data, version, applied = false) {
        this.type = type
        this.location = location
        this.data = data
        this.version = version + 1
        this.applied = applied
    }

}

export class Insertion extends Transformation {

    constructor(location, data, version, applied = false) {
        super('insert', location, data, version, applied)
    }

}

export class Deletion extends Transformation {

    constructor(location, data, version, applied = false) {
        super('delete', location, data, version, applied)
        this.dataLength = data.length.toString()
    }

}

export function createTransformation(previousText, newText, cursorPosition, version) {
    let change = previousText.length - newText.length
    var diff = new Diff()
    var textDiff = diff.main(previousText, newText)
    
    if (change < 0) {

        let inserted = textDiff.filter((value) => {
          return value[0] > 0
        })[0][1]

        cursorPosition = cursorPosition + change

        return new Insertion(cursorPosition, inserted, version, true)

      } else if (change > 0) {

        let deleted = textDiff.filter((value) => {
          return value[0] < 0
        })[0][1]

        return new Deletion(cursorPosition, deleted, version, true)
      }

      return null

}