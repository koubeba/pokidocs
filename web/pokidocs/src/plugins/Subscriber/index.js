import * as JSONHelper from '../../document/jsonHelper';

// The socket must already be initialized and available as this.$socket

const Subscriber = { 
  install (Vue, options) {
    Vue.prototype.$requestDocsInfo = async () => {
      Vue.prototype.$socket.send(JSONHelper.requestDocsInfoJSON());
    };

    Vue.prototype.$subscribeToDocument = async (documentID) => {
      Vue.prototype.$socket.send(JSONHelper.subscribeToDocJSON(documentID));
    };

    Vue.prototype.$insert = async (docID, location, data, version) => {
      Vue.prototype.$socket.send(JSONHelper.insertTransformJSON(docID, location, data, version))
    };

    Vue.prototype.$insertTransformation = async (docID, insertion, version) => {
      Vue.prototype.$socket.send(JSONHelper.insertionJSON(docID, insertion, version))
    };

    Vue.prototype.$delete = async (docID, location, data, version) => {
      Vue.prototype.$socket.send(JSONHelper.deleteTransformJSON(docID, location, data, version))
    };

    Vue.prototype.$deleteTransformation = async (docID, deletion, version) => {
      Vue.prototype.$socket.send(JSONHelper.deletionJSON(docID, deletion, version))
    };

    Vue.prototype.$unsubscribe = async () => {
      Vue.prototype.$socket.send(JSONHelper.unsubscribeJSON());
    };

    Vue.prototype.$requestBlob = async (documentID) => {
      Vue.prototype.$socket.send(JSONHelper.requestBlobJSON(documentID));
    };

    Vue.prototype.$createDoc = async (name) => {
      Vue.prototype.$socket.send(JSONHelper.createDocJSON(name));
    };

    Vue.prototype.$deleteDoc = async (documentID) => {
      Vue.prototype.$socket.send(JSONHelper.deleteDocJSON(documentID));
    };
  }
}

export default Subscriber;