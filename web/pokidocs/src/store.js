import Vue from 'vue'
import Vuex from 'vuex'
import {
  SOCKET_ONOPEN,
  SOCKET_ONCLOSE,
  SOCKET_ONERROR,
  SOCKET_ONMESSAGE,
  SOCKET_RECONNECT,
  SOCKET_RECONNECT_ERROR,
  TEXT_UNCHANGED,
  TEXT_CHANGED,
  TEXT_BUFFERED,
  TEXT_APPLYING
} from './mutation-types'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    socket: {
      isConnected: false,
      message: '',
      reconnectError: false,
    },
    textState: ''
  },
  getters: {
    getConnectionState: state => () => state.socket.isConnected,
    getTextState: state => () => state.textState
  },
  mutations: {
    [SOCKET_ONOPEN](state)  {
      state.socket.isConnected = true
    },
    [SOCKET_ONCLOSE](state)  {
      state.socket.isConnected = false
    },
    [SOCKET_ONERROR]()  {
    },
    // default handler called for all methods
    [SOCKET_ONMESSAGE](state, message)  {
      state.socket.message = message
    },
    // mutations for reconnect methods
    [SOCKET_RECONNECT]() {
    },
    [SOCKET_RECONNECT_ERROR](state) {
      state.socket.reconnectError = true;
    },
    TEXT_UNCHANGE(state) {
      state.textState = TEXT_UNCHANGED
    },
    TEXT_CHANGE(state) {
      state.textState = TEXT_CHANGED
    },
    TEXT_BUFFER(state) {
      state.textState = TEXT_BUFFERED
    },
    TEXT_APPLY(state) {
      state.textState = TEXT_APPLYING
    }

  },
  actions: {
    
  }
})
