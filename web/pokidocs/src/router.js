import Vue from 'vue'
import Router from 'vue-router'

import Files from './components/Files.vue'
import Edit from './components/Edit.vue'
import Home from './components/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/files',
      name: 'files',
      component: Files
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: Edit
    }
  ]
})
