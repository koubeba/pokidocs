import sys
import config
import time
from PySide2.QtWidgets import QApplication
from widgets import main_window

if __name__ == '__main__':
    """
    Główny skrypt uruchamiający aplikację Qt renderdocka.
    """
    try:
        qt_app = QApplication(sys.argv)

        main_view = main_window.MainWindow()
        main_view.show()

        qt_app.exec_()

    except Exception:
        config.logger.error("Problem with running qt app.", exc_info=True)


    