# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/uis/main_view.ui',
# licensing of 'ui/uis/main_view.ui' applies.
#
# Created: Tue Jun  4 14:44:35 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(640, 480)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(640, 480))
        MainWindow.setMaximumSize(QtCore.QSize(1920, 1080))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.create_document_button = QtWidgets.QPushButton(self.centralwidget)
        self.create_document_button.setObjectName("create_document_button")
        self.horizontalLayout_2.addWidget(self.create_document_button)
        self.delete_document_button = QtWidgets.QPushButton(self.centralwidget)
        self.delete_document_button.setObjectName("delete_document_button")
        self.horizontalLayout_2.addWidget(self.delete_document_button)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.horizontalLayout_2.setStretch(0, 1)
        self.horizontalLayout_2.setStretch(1, 1)
        self.horizontalLayout_2.setStretch(2, 4)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.line_4 = QtWidgets.QFrame(self.centralwidget)
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.verticalLayout_3.addWidget(self.line_4)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.verticalLayout.setObjectName("verticalLayout")
        self.document_list_label = QtWidgets.QLabel(self.centralwidget)
        self.document_list_label.setMaximumSize(QtCore.QSize(240, 16777215))
        self.document_list_label.setObjectName("document_list_label")
        self.verticalLayout.addWidget(self.document_list_label)
        self.document_list = QtWidgets.QListWidget(self.centralwidget)
        self.document_list.setObjectName("document_list")
        self.verticalLayout.addWidget(self.document_list)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout.addWidget(self.line)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.document_text_label = QtWidgets.QLabel(self.centralwidget)
        self.document_text_label.setObjectName("document_text_label")
        self.verticalLayout_2.addWidget(self.document_text_label)
        self.document_text = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.document_text.setObjectName("document_text")
        self.verticalLayout_2.addWidget(self.document_text)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.horizontalLayout.setStretch(2, 1)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.gridLayout_2.addLayout(self.verticalLayout_3, 0, 1, 1, 1)
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.gridLayout_2.addWidget(self.line_3, 0, 2, 1, 1)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout_2.addWidget(self.line_2, 0, 0, 1, 1)
        self.line_5 = QtWidgets.QFrame(self.centralwidget)
        self.line_5.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_5.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_5.setObjectName("line_5")
        self.gridLayout_2.addWidget(self.line_5, 1, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.action_exit = QtWidgets.QAction(MainWindow)
        self.action_exit.setObjectName("action_exit")
        self.action_save_file = QtWidgets.QAction(MainWindow)
        self.action_save_file.setObjectName("action_save_file")
        self.action_reconnect = QtWidgets.QAction(MainWindow)
        self.action_reconnect.setObjectName("action_reconnect")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "PokiDocs", None, -1))
        self.create_document_button.setText(QtWidgets.QApplication.translate("MainWindow", "Create document", None, -1))
        self.delete_document_button.setText(QtWidgets.QApplication.translate("MainWindow", "Delete document", None, -1))
        self.document_list_label.setText(QtWidgets.QApplication.translate("MainWindow", "List of documents:", None, -1))
        self.document_text_label.setText(QtWidgets.QApplication.translate("MainWindow", "Document:", None, -1))
        self.action_exit.setText(QtWidgets.QApplication.translate("MainWindow", "Exit", None, -1))
        self.action_save_file.setText(QtWidgets.QApplication.translate("MainWindow", "Save file..", None, -1))
        self.action_reconnect.setText(QtWidgets.QApplication.translate("MainWindow", "Reconnect..", None, -1))

