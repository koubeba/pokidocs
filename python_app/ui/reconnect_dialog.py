# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/uis/reconnect_dialog.ui',
# licensing of 'ui/uis/reconnect_dialog.ui' applies.
#
# Created: Tue Jun  4 14:44:35 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_ReconnectDialog(object):
    def setupUi(self, ReconnectDialog):
        ReconnectDialog.setObjectName("ReconnectDialog")
        ReconnectDialog.resize(400, 120)
        ReconnectDialog.setMinimumSize(QtCore.QSize(400, 120))
        ReconnectDialog.setMaximumSize(QtCore.QSize(400, 120))
        self.gridLayout = QtWidgets.QGridLayout(ReconnectDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.info_label = QtWidgets.QLabel(ReconnectDialog)
        self.info_label.setObjectName("info_label")
        self.verticalLayout.addWidget(self.info_label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.close_button = QtWidgets.QPushButton(ReconnectDialog)
        self.close_button.setObjectName("close_button")
        self.horizontalLayout.addWidget(self.close_button)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.reconnect_button = QtWidgets.QPushButton(ReconnectDialog)
        self.reconnect_button.setObjectName("reconnect_button")
        self.horizontalLayout.addWidget(self.reconnect_button)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(ReconnectDialog)
        QtCore.QMetaObject.connectSlotsByName(ReconnectDialog)

    def retranslateUi(self, ReconnectDialog):
        ReconnectDialog.setWindowTitle(QtWidgets.QApplication.translate("ReconnectDialog", "Reconnect", None, -1))
        self.info_label.setText(QtWidgets.QApplication.translate("ReconnectDialog", "<html><head/><body><p><span style=\" font-weight:600;\">Connection broken:<br/>1. Click \'reconnect\' to try to establish new connection.<br/>2. Click \'close\' to exit application.</span></p></body></html>", None, -1))
        self.close_button.setText(QtWidgets.QApplication.translate("ReconnectDialog", "Close", None, -1))
        self.reconnect_button.setText(QtWidgets.QApplication.translate("ReconnectDialog", "Reconnect", None, -1))

