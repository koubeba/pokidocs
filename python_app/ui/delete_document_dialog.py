# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/uis/delete_document_dialog.ui',
# licensing of 'ui/uis/delete_document_dialog.ui' applies.
#
# Created: Tue Jun  4 14:44:36 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_DeleteDocument(object):
    def setupUi(self, DeleteDocument):
        DeleteDocument.setObjectName("DeleteDocument")
        DeleteDocument.resize(240, 150)
        DeleteDocument.setMinimumSize(QtCore.QSize(240, 150))
        DeleteDocument.setMaximumSize(QtCore.QSize(240, 150))
        self.gridLayout = QtWidgets.QGridLayout(DeleteDocument)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(DeleteDocument)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.document_name = QtWidgets.QLineEdit(DeleteDocument)
        self.document_name.setObjectName("document_name")
        self.verticalLayout.addWidget(self.document_name)
        self.error_label = QtWidgets.QLabel(DeleteDocument)
        self.error_label.setText("")
        self.error_label.setObjectName("error_label")
        self.verticalLayout.addWidget(self.error_label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.cancel_button = QtWidgets.QPushButton(DeleteDocument)
        self.cancel_button.setObjectName("cancel_button")
        self.horizontalLayout.addWidget(self.cancel_button)
        self.ok_button = QtWidgets.QPushButton(DeleteDocument)
        self.ok_button.setObjectName("ok_button")
        self.horizontalLayout.addWidget(self.ok_button)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout_2.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.retranslateUi(DeleteDocument)
        QtCore.QMetaObject.connectSlotsByName(DeleteDocument)

    def retranslateUi(self, DeleteDocument):
        DeleteDocument.setWindowTitle(QtWidgets.QApplication.translate("DeleteDocument", "Delete document", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("DeleteDocument", "Enter document index:", None, -1))
        self.cancel_button.setText(QtWidgets.QApplication.translate("DeleteDocument", "Cancel", None, -1))
        self.ok_button.setText(QtWidgets.QApplication.translate("DeleteDocument", "Ok", None, -1))

