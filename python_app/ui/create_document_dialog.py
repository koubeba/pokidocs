# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/uis/create_document_dialog.ui',
# licensing of 'ui/uis/create_document_dialog.ui' applies.
#
# Created: Tue Jun  4 14:44:36 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_CreateDocument(object):
    def setupUi(self, CreateDocument):
        CreateDocument.setObjectName("CreateDocument")
        CreateDocument.resize(240, 150)
        CreateDocument.setMinimumSize(QtCore.QSize(240, 150))
        CreateDocument.setMaximumSize(QtCore.QSize(240, 150))
        self.gridLayout = QtWidgets.QGridLayout(CreateDocument)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(CreateDocument)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.document_name = QtWidgets.QLineEdit(CreateDocument)
        self.document_name.setObjectName("document_name")
        self.verticalLayout.addWidget(self.document_name)
        self.error_label = QtWidgets.QLabel(CreateDocument)
        self.error_label.setText("")
        self.error_label.setObjectName("error_label")
        self.verticalLayout.addWidget(self.error_label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.cancel_button = QtWidgets.QPushButton(CreateDocument)
        self.cancel_button.setObjectName("cancel_button")
        self.horizontalLayout.addWidget(self.cancel_button)
        self.ok_button = QtWidgets.QPushButton(CreateDocument)
        self.ok_button.setObjectName("ok_button")
        self.horizontalLayout.addWidget(self.ok_button)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout_2.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.retranslateUi(CreateDocument)
        QtCore.QMetaObject.connectSlotsByName(CreateDocument)

    def retranslateUi(self, CreateDocument):
        CreateDocument.setWindowTitle(QtWidgets.QApplication.translate("CreateDocument", "Create document", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("CreateDocument", "Enter new document name:", None, -1))
        self.cancel_button.setText(QtWidgets.QApplication.translate("CreateDocument", "Cancel", None, -1))
        self.ok_button.setText(QtWidgets.QApplication.translate("CreateDocument", "Ok", None, -1))

