import config
import json
import os
import socket
from connection.connection_manager import ConnectionManager
from workers.listening_worker import ListenerWorker
from ui.main_ui import Ui_MainWindow
from widgets import dialogs
from widgets.dialogs import ReconnectDialog
from PySide2 import QtGui, QtCore, QtWidgets
from PySide2.QtCore import Signal, QThread, QTimer

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, parent = None):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self._input_stopped = None
        self._load_payload = None

        self.connection_manager = ConnectionManager()
        self.refresh_docs_list_timer = None
        self.refresh_doc_text_timer = None

        self.reconnect_view = None
        self.create_document_view = None
        self.delete_document_view = None
        self.document_list_name_id = dict()
        self.list_id_document_id = dict()

        self.ui.create_document_button.clicked.connect(self.run_create_document_dialog)
        self.ui.delete_document_button.clicked.connect(self.run_delete_document_dialog)

        self.ui.document_text.document().contentsChange.connect(self.collect_changes)
        self.ui.document_list.itemDoubleClicked.connect(self.document_selected)

        self.start_cursor_position = None
        self.end_cursor_position = None

        self.current_document_info = dict()
        self.outgoing_changes = []
        self.incoming_changes = []
        self.outgoing_changes_history = []

        self.listener_thread = None
        self.listening_worker = None

        self.create_listener_thread()
        self.connect_socket()

    def create_listener_thread(self):
        self.listener_thread = QThread()
        self.listening_worker = ListenerWorker(self.connection_manager)
        self.listening_worker.moveToThread(self.listener_thread)

        self.listener_thread.started.connect(self.listening_worker.run)
        self.listening_worker.data_received.connect(self.receive_data)
        self.listening_worker.server_stopped.connect(self.close_server_connection)

    def run_reconnect_dialog(self):
        self.reconnect_view = dialogs.ReconnectDialog()
        self.reconnect_view.setModal(True)
        self.reconnect_view.ui.close_button.clicked.connect(self.close)
        self.reconnect_view.ui.reconnect_button.clicked.connect(self.connect_socket)
        self.reconnect_view.show()

    def run_create_document_dialog(self):
        self.create_document_view = dialogs.CreateDocumentDialog()
        self.create_document_view.setModal(True)
        self.create_document_view.ui.ok_button.clicked.connect(self.send_create_document_request)
        self.create_document_view.show()

    def run_delete_document_dialog(self):
        self.delete_document_view = dialogs.DeleteDocumentDialog()
        self.delete_document_view.setModal(True)
        self.delete_document_view.ui.ok_button.clicked.connect(self.send_delete_document_request)
        self.delete_document_view.show()

    @QtCore.Slot()
    def connect_socket(self):
        try:
            self.connection_manager.create_and_connect()
            self.create_listener_thread()
            self.listener_thread.start()
            self.ui.document_text.setReadOnly(True)

            QTimer.singleShot(3000, self.populate_docs_info)

            self.refresh_docs_list_timer = QTimer()
            self.refresh_docs_list_timer.timeout.connect(self.populate_docs_info)
            self.refresh_docs_list_timer.start(10000)

            # self.refresh_doc_text_timer = QTimer()
            # self.refresh_doc_text_timer.timeout.connect(self.refresh_doc_text)
            # self.refresh_doc_text_timer.start(15000)

            if self.reconnect_view:
                self.reconnect_view.hide()
                self.reconnect_view = None

        except socket.error:    
            config.logger.error("Problem with connection to server.", exc_info=True)

            if not self.reconnect_view:
                self.run_reconnect_dialog()

    @QtCore.Slot()
    def send_create_document_request(self):
        doc_name = self.create_document_view.ui.document_name.text()
        document_data = {
            "name": doc_name
        }
        self.connection_manager.create_document(document_data)
        QTimer.singleShot(2, self.populate_docs_info)

    @QtCore.Slot()
    def send_delete_document_request(self):
        list_id = self.delete_document_view.ui.document_name.text()

        doc_id = self.list_id_document_id[list_id]
        document_data = {
            "document_id": int(doc_id)
        }
        if self.current_document_info and  int(doc_id) == self.current_document_info["id"]:
            self.connection_manager.unsubscribe_document(int(doc_id))
        self.connection_manager.delete_document(document_data)
        QTimer.singleShot(2, self.populate_docs_info)

    def populate_docs_info(self):
        self.connection_manager.ask_for_documents_info()

    def refresh_doc_text(self):
        if "id" in self.current_document_info:
            self.connection_manager.get_document_data(self.current_document_info["id"])

    @QtCore.Slot()
    def receive_data(self, message):
        config.logger.info(message)

        if "typeof" in message:
            message_type = message["typeof"]

            if message_type == "error":
                if message["data"]["code"] == 600:
                    self.stop_listening_thread()
                    self.close_socket()
                    self.run_reconnect_dialog()

            elif message_type == "docsinfo":
                docs_info = message["data"]["docs_info"]
                self.set_document_list_info(docs_info)
                
            elif message_type == "blob":
                doc_data = message["data"]
                self.set_subscribed_document_text(doc_data)

            elif message_type == "transform":
                self.incoming_changes.append(message["data"])

                self.ui.document_text.document().contentsChange.disconnect(self.collect_changes)
                transform_data = message["data"]
                self.current_document_info["version"] = transform_data["version"]
                if transform_data["type"] == "insert":
                    location = transform_data["location"]
                    document_id = transform_data["document_id"]
                    insert_change = transform_data["data"]

                    cursor = self.ui.document_text.textCursor()
                    cursor_position = cursor.position()
                    cursor.setPosition(location)
                    cursor.insertText(insert_change)
                    if location <= cursor_position:
                        cursor.setPosition(len(insert_change) + cursor_position)
                    else:
                        cursor.setPosition(cursor_position)

                else:
                    location = transform_data["location"]
                    document_id = transform_data["document_id"]
                    delete_change = transform_data["data"]

                    cursor = self.ui.document_text.textCursor()
                    cursor_position = cursor.position()
                    cursor.setPosition(location)
                    for _ in range(int(delete_change)):
                        cursor.deleteChar()
                    if location <= cursor_position:
                        cursor.setPosition(location + cursor_position)
                    else:
                        cursor.setPosition(location)

                self.ui.document_text.document().contentsChange.connect(self.collect_changes)

    def set_document_list_info(self, docs_info):
        if len(docs_info) != self.ui.document_list.count():
            self.ui.document_list.clear()
            i = 1
            for doc_number in docs_info: 
                config.logger.info(str(docs_info))
                config.logger.info(doc_number)
                doc_name = docs_info[doc_number]
                self.list_id_document_id[str(i)] = doc_number
                self.ui.document_list.addItem(str(i) + ". " + doc_name)
                i += 1

    def set_subscribed_document_text(self, doc_data):
        cursor = self.ui.document_text.textCursor()
        cursor_position = cursor.position()
        config.logger.info(cursor_position)

        self.current_document_info = {
            "id": doc_data["document_id"],
            "version": doc_data["version"],
        }

        self.ui.document_text.document().contentsChange.disconnect(self.collect_changes)
        self.ui.document_text.setPlainText(doc_data["payload"])
        self.ui.document_text.document().contentsChange.connect(self.collect_changes)


        self.ui.document_text.setReadOnly(False)
        
        config.logger.info(cursor_position)
        cursor = self.ui.document_text.textCursor()
        cursor.setPosition(cursor_position)

        config.logger.info(self.current_document_info)

    @QtCore.Slot()
    def document_selected(self, selected_document):
        document_name = selected_document.text()
        if "id" in self.current_document_info:
            self.connection_manager.unsubscribe_document(self.current_document_info["id"]) 
        doc_id =  int(self.list_id_document_id[str(self.ui.document_list.currentRow() + 1)])
        self.connection_manager.subscribe_document(doc_id)

    @QtCore.Slot()
    def collect_changes(self, position, chars_removed, chars_added):
        config.logger.info("In show changes.")
        config.logger.info(str(position) + "----" + str(chars_removed) + "----" + str(chars_added))

        if chars_removed:
            change_delete = (position, "delete", chars_removed)
            config.logger.info("Delete -> " + str(change_delete))
            self.outgoing_changes.append(change_delete)
            QTimer.singleShot(3500, self.group_and_send_changes)
        
        if chars_added:
            change_add = (position, "insert", chars_added, self.ui.document_text.toPlainText()[position: position + chars_added])
            config.logger.info("Insert -> " + str(change_add))
            self.outgoing_changes.append(change_add)
            QTimer.singleShot(3500, self.group_and_send_changes)

    @QtCore.Slot()
    def group_and_send_changes(self):
        config.logger.info("In collect_changes" + str(self.outgoing_changes))

        if self.outgoing_changes:

            config.logger.info(self.outgoing_changes)

            first_change = self.outgoing_changes[0]
            location = first_change[0]
            previous_position = first_change[0] 
            modification_type = first_change[1]
            
            if modification_type == "insert":
                payload_change = first_change[3]
            else:
                payload_change = first_change[2]
            config.logger.info(self.outgoing_changes)
            config.logger.info(self.outgoing_changes[1:])

            self.outgoing_changes[:] = self.outgoing_changes[1:]

            skip_counter = 0

            for change in self.outgoing_changes:
                if modification_type == "insert":
                    if change[1] == modification_type and change[0] - change[2] - previous_position == 0:
                        payload_change += change[3]
                        skip_counter += 1
                        previous_position = change[0]
                    else:
                        break
                else:
                    if change[1] == modification_type and change[0] + change[2] - previous_position == 0:
                        location = change[0]
                        payload_change += 1
                        skip_counter += 1
                        previous_position = change[0]
                    else:
                        break  

            if modification_type == "delete":
                payload_change = str(payload_change)

            self.outgoing_changes[:] = self.outgoing_changes[skip_counter:]

            self.current_document_info["version"] = self.current_document_info["version"] + 1

            modification_data = {
                "type": modification_type,
                "document_id": self.current_document_info["id"],
                "location": location,
                "data": payload_change,
                "version": self.current_document_info["version"]
            }    

            self.outgoing_changes_history.append(modification_data)

            self.connection_manager.send_document_modification(modification_data)
            
            config.logger.info(self.outgoing_changes)
            config.logger.info(payload_change)

    def close_server_connection(self):
        self.stop_listening_thread()
        self.close_socket()
        self.run_reconnect_dialog()

    def close_socket(self):
        config.logger.info("Close socket...")

        self.refresh_docs_list_timer.stop()
        # self.refresh_doc_text_timer.stop()

        self.connection_manager.close_connection()

    def stop_listening_thread(self):
        config.logger.info("Stop listening thread...")

        if not self.listener_thread.isFinished():
            self.listening_worker.stop_listening_worker()
            self.listener_thread.quit()
            self.listener_thread.wait()

    def closeEvent(self, event):
        """
        Funkcja QWidget wywołana w momencie zamykania aplikacji Qt.

        """
        config.logger.info("Main window close event...")

        self.stop_listening_thread()
        self.close_socket()

        self.deleteLater()