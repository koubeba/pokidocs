
from PySide2 import QtWidgets
from ui.reconnect_dialog import Ui_ReconnectDialog
from ui.create_document_dialog import Ui_CreateDocument
from ui.delete_document_dialog import Ui_DeleteDocument

class ReconnectDialog(QtWidgets.QDialog):
    def __init__(self, parent = None):
        super().__init__()

        self.ui = Ui_ReconnectDialog()
        self.ui.setupUi(self)
    
        self.ui.close_button.clicked.connect(self.close)

class CreateDocumentDialog(QtWidgets.QDialog):
    def __init__(self, parent = None):
        super().__init__()

        self.ui = Ui_CreateDocument()
        self.ui.setupUi(self)
    
        self.ui.cancel_button.clicked.connect(self.close)

class DeleteDocumentDialog(QtWidgets.QDialog):
    def __init__(self, parent = None):
        super().__init__()

        self.ui = Ui_DeleteDocument()
        self.ui.setupUi(self)
    
        self.ui.cancel_button.clicked.connect(self.close)