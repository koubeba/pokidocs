import errno
import time
import select
import sys
import ssl
import signal
import socket
import json
import config
import threading
import traceback
from functools import partial

class ConnectionManager():
    def _init__(self):
        self.tcp_socket = None

    def create_and_connect(self):
        try:
            context = ssl._create_unverified_context()
            raw_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            raw_socket.connect((config.ADDRESS, config.PORT))


            self.tcp_socket = context.wrap_socket(raw_socket, do_handshake_on_connect = False)
            self.tcp_socket.setblocking(False)

            config.logger.info("Creating and connecting socket.")

        except socket.error as exc:
            raise exc
            
    def _create_message_to_send(self, data):
        try:
            message = json.dumps(data)
            message = message.encode() + b"\x03"
            return message
        except Exception as ex:
            raise ex

    def decode_message_from_server(self, message):
        try:
            message = message.decode()
            data_set = json.loads(message)
            return data_set
        except Exception as ex:
            raise ex

    def create_document(self, document_data):
        try:
            data_set = {
                "typeof": "createdoc",
                "data": document_data
            }
            config.logger.info("Send create document request: " + str(document_data))
            self.tcp_socket.sendall(self._create_message_to_send(data_set))
        except Exception as ex:
            traceback.print_exc()

    def delete_document(self, document_data):
        try:
            data_set = {
                "typeof": "deletedoc",
                "data": document_data
            }
            config.logger.info("Send delete document request: " + str(document_data))
            self.tcp_socket.sendall(self._create_message_to_send(data_set))
        except Exception as ex:
            traceback.print_exc()

    def send_document_modification(self, modification_data):
        try:
            data_set = {
                "typeof": "transform",
                "data": modification_data
            }
            config.logger.info("Send modification:\n" + str(modification_data))
            self.tcp_socket.sendall(self._create_message_to_send(data_set))
        except Exception as ex:
            traceback.print_exc()

    def ask_for_documents_info(self):
        try:
            data_set = {
                "typeof": "requestdocsinfo",
                "data": dict()
            }
            config.logger.info("Ask for documents info.")
            self.tcp_socket.sendall(self._create_message_to_send(data_set))
        except:
            traceback.print_exc()

    def subscribe_document(self, document_id):
        try:
            data_set = {
                "typeof": "subscribe",
                "data": {
                    "document_id": document_id
                }
            }
            config.logger.info("Subscribe document with id -> " + str(document_id))
            self.tcp_socket.sendall(self._create_message_to_send(data_set))
        except:
            traceback.print_exc()

    def unsubscribe_document(self, document_id):
        try:
            data_set = {
                "typeof": "unsubscribe",
                "data": {}
            }
            config.logger.info("Unsubscribe document with id -> " + str(document_id))
            self.tcp_socket.sendall(self._create_message_to_send(data_set))
        except:
            traceback.print_exc()

    def get_document_data(self, document_id):
        try:
            data_set = {
                "typeof": "requestblob",
                "data": {
                    "document_id": document_id
                }
            }
            config.logger.info("Get data for document with id -> " + str(document_id))
            self.tcp_socket.sendall(self._create_message_to_send(data_set))
        except:
            traceback.print_exc()

    def receive_message_part(self):
        '''
        Metoda wywołująca metode recv() obiektu socket.
        Jako, ze korzystamy z non-blocking socket, w przypadku gdy nie ma danych z serwera rzucany
        jest wyjątek EAGAIN/EWOULDBLOCK. W przypadku gdy połączenie upadnie, 
        recv() zwraca pusty ciąg znaków.
        '''
        try:
            return self.tcp_socket.recv(config.PACKET_SIZE)
        except ssl.SSLWantReadError as ssl_exc:
                raise ssl_exc
                
        except socket.error as sock_exc:
            raise sock_exc

    def close_connection(self):
        '''
        Metoda zamykająca połączenie tcp.
        '''
        try:
            if self.tcp_socket:
                self.tcp_socket.close()
                config.logger.info("Closing socket.")
        except socket.error as exc:
            raise exc
