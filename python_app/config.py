
import logging
import logging.handlers
import sys

log_level = logging.DEBUG

formatter = logging.Formatter("== [%(levelname)s %(asctime)s] in %(threadName)s [%(filename)s:%(lineno)s - %(funcName)s()] \n%(message)s")

enviroment = 'development'

logger = logging.getLogger(enviroment)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setFormatter(formatter)
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)

ADDRESS = '25.71.147.121'
PORT = 7778
PACKET_SIZE = 4096
DELIMITER = b'\x03'