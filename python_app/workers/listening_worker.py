import config
import errno
import socket
import select
import ssl
from PySide2 import QtCore
from PySide2.QtCore import Signal

class ListenerWorker(QtCore.QObject):

    data_received = QtCore.Signal(dict)
    server_stopped = QtCore.Signal()

    def __init__(self, connection_manager):
        super().__init__()
        self.connection_manager = connection_manager
        self._stopped = False

    @QtCore.Slot()
    def run(self):
        '''
        Metoda uruchamiana przy starcie QThread. Uruchamia w pętli metodę ConnectManager odczytującą dane z serwera.
        Zależnie od rzuconego błędu bądź braku danych usypia na określony czas działanie wątku bądź wysyła sygnał
        o upadku połączenia z serwerem. 
        '''
        received_message = b''
        while not self._stopped:
            while not self._stopped and config.DELIMITER not in received_message:
                try:
                    received_part = self.connection_manager.receive_message_part()
                    received_message += received_part
                    config.logger.info(received_message)
                    
                    if received_message == b'':
                        self.server_stopped.emit()
                        QtCore.QThread.sleep(2)

                except ssl.SSLWantReadError:
                        config.logger.info("EAGAIN, EWOULDBLOCK")
                        QtCore.QThread.sleep(2)


                except socket.error as exc:
                    exception = exc.args[0]
                    if exception == errno.EAGAIN or exception == errno.EWOULDBLOCK:
                        config.logger.info("EAGAIN, EWOULDBLOCK")
                        QtCore.QThread.sleep(2)
                    else:
                        config.logger.info("Caught exception socket.error : %s" % exc)                        
                        raise exc

            if received_message != b'':
                message, received_message = received_message.split(b'\x03', 1)
                try:
                    message_to_emit = self.connection_manager.decode_message_from_server(message)
                    config.logger.info(message_to_emit)
                    self.data_received.emit(message_to_emit)
                except Exception as exc:
                    raise exc

        config.logger.info("Finished worker!")

    """ 
    Used as method instead of slot because of -> 
    "Signals between threads are transmitted (asynchronously) via the receiving thread's event loop.
    Hence responsiveness of GUI or any thread = ability to process events.
    E.g., if a thread is busy in a function loop, it can't process events, so it won't respond to signals from the GUI until the function returns."
    link -> https://stackoverflow.com/questions/41526832/pyqt5-qthread-signal-not-working-gui-freeze
    """
    def stop_listening_worker(self):
        config.logger.info("In stop_listening_worker")
        self._stopped = True




